let
  type     box = { value : int }
  var      box := box { value = 51 }
in
  box.head
end

#!/usr/bin/env ruby

require 'rubygems'
require 'open3'
require_relative "ruby_files/g_test.rb"
require_relative "ruby_files/print_list.rb"


def print_group_test(name, global_g)
  group = G_Test.new
  group.name = name.upcase
  group.previous
  l = Dir["tests/#{name}/*"]
  group.number = l.length
  if "#{name}" == "tests_LRDE"
    print_tests(l, group, "", "0")
  elsif "#{name}" == "parser_ok"
    print_tests(l, group, "-X --parse", "0")
  elsif "#{name}" == "lexer_ok"
    print_tests(l, group, "-X --parse", "0")
  elsif "#{name}" == "parser_ko"
    print_tests(l, group, "", "3")
  elsif "#{name}" == "lexer_ko"
    print_tests(l, group, "", "2")
  elsif "#{name}" == "error_1"
    print_tests(l, group, "", "1")
  elsif "#{name}" == "ast"
    print_tests(l, group, "-A", "0")
  elsif "#{name}" == "syntax"
    print_tests(l, group, "", "3")
  elsif "#{name}" == "object_without_option"
    print_tests(l, group, "", "2" )
  elsif "#{name}" == "object_with_option"
    print_tests(l, group, "-o", "0")
  elsif "#{name}" == "bind_ok"
    print_tests(l, group, "-bBA", "0")
  elsif "#{name}" == "bind_ko"
    print_tests(l, group, "-bBA", "4")
  elsif "#{name}" == "renamer_ok"
    print_tests(l, group, "-X --rename -A", "0")
  elsif "#{name}" == "renamer_ko"
    print_tests(l, group, "-X --rename -A", "4")
  elsif "#{name}" == "escapes"
    print_tests(l, group, "-XEAeEA", "0")
  elsif "#{name}" == "type_ok"
    print_tests(l, group, "-T", "0")
  elsif "#{name}" == "type_ko"
    print_tests(l, group, "-T", "5")
  elsif "#{name}" == "object_type_ko"
    print_tests(l, group, "--object-types-compute", "5")
  end
  global_g.number += group.number
  global_g.succ += group.succ
  global_g.failed += group.failed
  group.end
end

def html_group_test(name, global_g)
  group = G_Test.new
  group.name = name.upcase
  group.previous
  l = Dir["tests/#{name}/*"]
  group.number = l.length
  if "#{name}" == "tests_LRDE"
    html_tests(name, l, group, "", "0")
  elsif "#{name}" == "parser_ok"
    html_tests(name, l, group, "-X --parse", "0")
  elsif "#{name}" == "lexer_ok"
    html_tests(name, l, group, "-X --parse", "0")
  elsif "#{name}" == "parser_ko"
    html_tests(name, l, group, "", "3")
  elsif "#{name}" == "lexer_ko"
    html_tests(name, l, group, "", "2")
  elsif "#{name}" == "error_1"
    html_tests(name, l, group, "", "1")
  elsif "#{name}" == "ast"
    html_tests(name, l, group, "-A", "0")
  elsif "#{name}" == "syntax"
    html_tests(name, l, group, "", "3")
  elsif "#{name}" == "object_without_option"
    html_tests(name, l, group, "", "2" )
  elsif "#{name}" == "object_with_option"
    html_tests(name, l, group, "-o", "0")
  elsif "#{name}" == "bind_ok"
    html_tests(name, l, group, "-bBA", "0")
  elsif "#{name}" == "bind_ko"
    html_tests(name, l, group, "-bBA", "4")
  elsif "#{name}" == "renamer_ok"
    html_tests(name, l, group, "-X --rename -A", "0")
  elsif "#{name}" == "renamer_ko"
    html_tests(name, l, group, "-X --rename -A", "4")
  elsif "#{name}" == "escapes"
    html_tests(name, l, group, "-XEAeEA", "0")
  elsif "#{name}" == "type_ok"
    html_tests(name, l, group, "-T", "0")
  elsif "#{name}" == "type_ko"
    html_tests(name, l, group, "-T", "5")
  elsif "#{name}" == "object_type_ko"
    html_tests(name, l, group, "--object-types-compute", "5")
  end
end

def display_info()
  puts "Choose a part of the testsuite:"
  puts "\tthe whole testsuite -> all"
  puts "\ttests provided by the LRDE -> lrde"
  puts "\ttests provided by the LRDE in HTML -> html lrde"
  puts "\ttests of TCn -> n (replace n by the right number)"
  puts "\ttests of TCn in HTML -> html n (replace n by the right number)"
  puts "\tleave the testsuite mode -> quit or ctrl+c"
  puts "\tjust admire our splendid Tigrou -> tigrou"
  puts "\tOthers inputs are invalid"
  puts ""
end

def read_input()
  until ARGV.empty? do
    ARGV.shift
  end
  art()
  display_info()
  a = gets
  a = a.chomp
  while a != "quit"
    if a == "lrde"
      global_g = G_Test.new
      lrde(global_g)
      global_g.end
    elsif a == "1"
      global_g = G_Test.new
      tc1(global_g)
      global_g.end
    elsif a == "2"
      global_g = G_Test.new
      tc2(global_g)
      global_g.end
    elsif a == "3"
      global_g = G_Test.new
      tc3(global_g)
      global_g.end
    elsif a == "4"
      global_g = G_Test.new
      tc4(global_g)
      global_g.end
    elsif a == "5"
      global_g = G_Test.new
      tc5(global_g)
      global_g.end
    elsif a == "all"
      global_g = G_Test.new
      all(global_g)
      global_g.end
    elsif a == "tigrou"
      art()
    elsif a == "html lrde"
      html_lrde(global_g)
    elsif a == "html 1"
      html_tc1(global_g)
    elsif a == "html 2"
      html_tc2(global_g)
    elsif a == "html 3"
      html_tc3(global_g)
    elsif a == "html 4"
      html_tc4(global_g)
    end
    display_info()
    a = gets
    a = a.chomp
  end
end


def art()
  puts "
           .,;\033[1;31m%%\033[0m,                                   .,\033[1;31m%%\033[0m;
         ;\033[1;31m%%%%%%%%\033[0m,     ...\033[1;31m%%%%%\033[0m::::::::::::::..   ,\033[1;31m%%% %\033[0m;
       ;\033[1;31m%%% %%%%%%%\033[0m ..:::\033[1;31m%%%%\033[0m:::::::\033[1;33m@@@@@@@@@@@@aa\033[0m.\033[1;31m%% %%%%\033[0m
       \033[1;31m%%%%%% %%%%%\033[0m:::::\033[1;31m%%%\033[0m::::\033[1;33m@@@@@@@@@@@@@@@@@@@@@\033[0m.\033[1;31m%%%%%\033[0m
       \033[1;31m%%%%%%% %%%%\033[0m:::\033[1;31m%%%\033[0m::::\033[1;33m@@@@@@@@@   @@@@@   @@@@@\033[0m.\033[1;31m%%\033[0m
        \033[1;31m%%%%%%%%%%%\033[0m::\033[1;31m%%\033[0m::::\033[1;33m@@@@@@@@@@ @@@ @@@ @@@ @@@@@\033[0m.
         `\033[1;31m%%%%\033[0m'':: ::\033[1;31m%%\033[0m:::\033[1;33m@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\033[0m
                  \033[1;31m%\033[0m::\033[1;31m%%%\033[0m::\033[1;33m@@@@@@@@@@@    @@@@@    @@@@@@\033[0m
                  \033[1;31m%%%\033[0m:\033[1;31m%%%\033[0m::\033[1;33m@@@@@@@@@@@aa@@@@@@@aa@@@@@@\033[0m          ,
                   \033[1;31m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\033[0m;,    ,
                    \033[1;31m%%%%%%%%\033[0m:::::\033[1;31m%%%%%%\033[0m\033[1;33maa@@@@@@@@\033[0m\033[1;35m^^^^^^^^^^^\033[0m ,
                      \033[1;31m%%%%%\033[0m:::\033[1;31m%%%%%\033[0m\033[1;33maa@@@@@@@@@@@\033[0m\033[1;35m^^^^^^^^^^^^^\033[0m , ' ' '
                        :\033[1;31m%%%%%%%%%\033[0m\033[1;33m@@@@@@@@@@@@@@@\033[0m\033[1;35m^^^^^^^^^^^\033[0m   ,
                       :::::::\033[1;31m%%%\033[0m\033[1;33m@@@@@@@@ @@@@@@@@@\033[0m\033[1;35m ^^^^^^^\033[0m      ,
                     :::::\033[1;31m%%%%%%%\033[0m\033[1;33m@@@@@@@  @@@@@@@@@@@@^^@@\033[0m
                   \033[1;31m%%%%%%%%%%%%%%%\033[0m\033[1;33m@@@@@@   @@@@@@@@@@@@@@\033[0m
                 ::::::\033[1;31m%%%%%%%%%%%%\033[0m\033[1;33m@@@@@,,  @@@@@@@@@@@\033[0m
               :::::::::::::\033[1;31m%%%%%%%\033[0m\033[1;33m@@@@@,,',    @@\033[0m
             ::::::::\033[1;31m%%%%%%%%%%%%%\033[0m \033[1;33m@@@@@, ,,    @@\033[0m
           \033[1;31m%%%%%%%%%%%%%%%%%%%%%%\033[0m  \033[1;33m@@@@@aaaaaaa@@\033[0m
      :::::::::\033[1;31m%%%%%%%%%%%%%%%%%\033[0m   \033[1;33m@@@@@@@@@@@@@\033[0m
    :::::::::::::::::::\033[1;31m%%%%%%%%%%%\033[0m  \033[1;33m@@@@@@@@@@@\033[0m
 ::::::::::::::\033[1;31m%%%%%%%%%%%%%%%%%%%%\033[0m
\033[1;31m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\033[0m"
  puts "\033[1;33m*************************************************************************"
  puts "*************************************************************************"
  puts "******                                                             ******"
  puts "******           ******  ******  ******   ******  ******           ******"
  puts "******             **      **    **       **      **  **           ******"
  puts "******             **      **    *   ***  ****    ******           ******"
  puts "******             **      **    *** **   **      ****             ******"
  puts "******             **    ******  ******   ******  **  **           ******"
  puts "******                                                             ******"
  puts "*************************************************************************"
  puts "*************************************************************************\033[0m"
end

def begin_ts()
  puts "\033[1;33m*************\033[0m"
  puts "\033[1;33m*\033[1;5m\033[32m BEGINNING\033[0m \033[1;33m*\033[0m"
  puts "\033[1;33m*************\033[0m\n\n"

end

def lrde(global_g)
  global_g.name = "LRDE TESTS"
  print_group_test("tests_LRDE", global_g)
end

def html_lrde(global_g)
  html_group_test("tests_LRDE", global_g)
end

def tc1(global_g)
  global_g.name = "TC1 TESTS"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**         ******  ******           **      **\033[0m"
  puts "\033[1;33m**           **   ***             * **      **\033[0m"
  puts "\033[1;33m**           **   ***               **      **\033[0m"
  puts "\033[1;33m**           **   ***               **      **\033[0m"
  puts "\033[1;33m**           **    ******           **      **\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m\n\n"

  print_group_test("parser_ok", global_g)
  print_group_test("lexer_ok", global_g)
  print_group_test("parser_ko", global_g)
  print_group_test("lexer_ko", global_g)
  print_group_test("error_1", global_g)
end

def html_tc1(global_g)
  html_group_test("parser_ok", global_g)
  html_group_test("lexer_ok", global_g)
  html_group_test("parser_ko", global_g)
  html_group_test("lexer_ko", global_g)
  html_group_test("error_1", global_g)
end

def tc2(global_g)
  global_g.name = "TC2 TESTS"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**         ******  ******         ****      **\033[0m"
  puts "\033[1;33m**           **   ***                **     **\033[0m"
  puts "\033[1;33m**           **   ***               **      **\033[0m"
  puts "\033[1;33m**           **   ***              **       **\033[0m"
  puts "\033[1;33m**           **    ******         *****     **\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m\n\n"

  print_group_test("ast", global_g)
  print_group_test("syntax", global_g)
  print_group_test("object_without_option", global_g)
  print_group_test("object_with_option", global_g)
end

def html_tc2(global_g)
  html_group_test("ast", global_g)
  html_group_test("syntax", global_g)
  html_group_test("object_without_option", global_g)
  html_group_test("object_with_option", global_g)
end

def tc3(global_g)
  global_g.name = "TC3 TESTS"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**         ******  ******         *****     **\033[0m"
  puts "\033[1;33m**           **   ***                **     **\033[0m"
  puts "\033[1;33m**           **   ***              ****     **\033[0m"
  puts "\033[1;33m**           **   ***                **     **\033[0m"
  puts "\033[1;33m**           **    ******         *****     **\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m\n\n"

  print_group_test("bind_ok", global_g)
  print_group_test("bind_ko", global_g)
  print_group_test("renamer_ok", global_g)
  print_group_test("renamer_ko", global_g)
end

def html_tc3(global_g)
  html_group_test("bind_ok", global_g)
  html_group_test("bind_ko", global_g)
  html_group_test("renamer_ok", global_g)
  html_group_test("renamer_ko", global_g)
end

def tc4(global_g)
  global_g.name = "TC4 TESTS"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**         ******  ******        **  **     **\033[0m"
  puts "\033[1;33m**           **   ***            **  **     **\033[0m"
  puts "\033[1;33m**           **   ***            ******     **\033[0m"
  puts "\033[1;33m**           **   ***                **     **\033[0m"
  puts "\033[1;33m**           **    ******            **     **\033[0m"
  puts "\033[1;33m**                                          **\033[0m"
  puts "\033[1;33m**********************************************\033[0m"
  puts "\033[1;33m**********************************************\033[0m\n\n"

  print_group_test("escapes", global_g)
  print_group_test("type_ok", global_g)
  print_group_test("type_ko", global_g)
  print_group_test("object_type_ko", global_g)
end

def html_tc4(global_g)
  html_group_test("escapes", global_g)
  html_group_test("type_ok", global_g)
  html_group_test("type_ko", global_g)
  html_group_test("object_type_ko", global_g)
end

def tc5(global_g)
  global_g.name = "TC5 TESTS"

end

def all(global_g)
  lrde(global_g)
  tc1(global_g)
  tc2(global_g)
  tc3(global_g)
  tc4(global_g)
  tc5(global_g)
  global_g.name = "TIGER TESTS"
end

def end_ts()
  puts "\033[1;33m*******\033[0m"
  puts "\033[1;33m*\033[1;5m\033[31m END\033[0m \033[1;33m*\033[0m"
  puts "\033[1;33m*******\033[0m"
end


read_input()

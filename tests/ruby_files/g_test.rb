#!/usr/bin/env ruby

require 'rubygems'
require 'open3'

class G_Test
  attr_accessor :name, :number, :succ, :failed, :list
  def initialize
    @name = "None"
    @number = 0
    @succ = 0
    @failed = 0
    @percentage = 0
  end
  def previous
    puts " #{@name} \033[0;33m:\033[0m\n"
    puts " \033[1;33m-----------------------\033[0m\n\n"
  end
  def compute_percentage
     succ = 0.0
     succ += @succ
     number = 0.0
     number += @number
     percentage = succ/number
     percentage *= 100
     @percentage = percentage 
  end
  def end
    puts "\n"
    compute_percentage
    puts " #{@name} \033[0;33m: [#{@percentage} %]\033[0m" 
    printf ""
    puts " \033[1;33m-----------------------\033[0m\n"
    puts " \033[0m Number of tests: \033[1;36m#{@number}\033[0m\n"
    puts " \033[0m Successful: \033[1;32m#{@succ}\033[0m\n"
    puts " \033[0m Failed: \033[1;31m#{@failed}\033[0m\n"
    puts " \033[1;33m-----------------------\033[0m\n\n"
  end
end

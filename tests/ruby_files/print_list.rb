#!/usr/bin/env ruby

require 'rubygems'
require 'open3'


def print_tests(list, group, options, ret)
  n = 1
  j = list.length
  until n>j
    n += 1
    name = list.pop
    out = ""
    exitstatus = ""
    if "#{name}" != "options"
      Open3.popen3("./src/tc #{options} #{name}") do |stdin, stdout, stderr, t|
        out = stderr.read
        exitstatus = t.value.exitstatus
      end
      if "#{exitstatus}" == "#{ret}"
        puts "   \033[1;33m#{name} :\033[0m [\033[1;32mOK\033[0m]\n\n"
        group.succ += 1
      else
        puts "   \033[1;33m#{name} :\033[0m [\033[1;31mKO\033[0m], \033[1;33mError: #{exitstatus}\033[0m\n\n"
        group.failed += 1
      end
    end
  end
end

def html_tests(dir_name, list, group, options, ret)
  fileHtml = File.new("test.html", "w")
  fileHtml.puts "<HTML>"
  fileHtml.puts "<HEAD>"
  fileHtml.puts "<style media='all' type='text/css'>"
  fileHtml.puts "body {font-family: Helvetica Neue, sans-serif; font-size: 12px; color: #525252; padding: 0; margin: 0;background: #f2f2f2;}"
  fileHtml.puts ".par { height: 5%; }"
  fileHtml.puts ".recap { font-size: 18px; }"
  fileHtml.puts ".column {float: left; width: 45%; padding: 7px; }"
  fileHtml.puts ".row:after {content: ""; display: table; clear: both; }"
  fileHtml.puts ".pass {color: #ffffff; background: #34d9a2; padding: 10px 20px 10px 20px; text-decoration: none; width:50px;}"
  fileHtml.puts ".fail {color: #ffffff; background: #f25e6a; padding: 10px 20px 10px 20px; text-decoration: none; width:50px;}"
  fileHtml.puts "</style>"
  fileHtml.puts "</HEAD>"
  fileHtml.puts "<BODY>"
  fileHtml.puts "<h2>Testsuite</h2>"
  fileHtml.puts "<DIV class='row'>"
  fileHtml.puts "<DIV class='column' style='background-color:#ddd;'>"
  fileHtml.puts "<h2>Tests</h2>"
  n = 1
  j = list.length
  until n>j
    n += 1
    name = list.pop
    out = ""
    exitstatus = ""
    if "#{name}" != "options"
      Open3.popen3("./src/tc #{options} #{name}") do |stdin, stdout, stderr, t|
        out = stderr.read
        exitstatus = t.value.exitstatus
      end
      if "#{exitstatus}" == "#{ret}"
        fileHtml.puts "<p class='par'>#{name}: <span class='pass'>Pass</span></p>"
        group.succ += 1
      else
        fileHtml.puts "<p class='par'>#{name}: <span class='fail'>Fail: Error #{exitstatus}</span></p>"
        group.failed += 1
      end
    end
  end
  fileHtml.puts "</DIV>"
  fileHtml.puts "<DIV class='column'>"
  fileHtml.puts "<h2>Recap</h2>"
  fileHtml.puts "<p><img src='tests/tigrou1.jpg'></p>"
  percent = group.succ * 100 / group.number
  fileHtml.puts "<p class='recap'>#{group.name} -> #{percent}%</br>Number of tests: #{group.number}</br>Successful: #{group.succ}</br>Failed: #{group.failed}</p>"
  fileHtml.puts "</DIV> </DIV>"
  fileHtml.puts "</BODY></HTML>"
  fileHtml.close()
  system("firefox test.html")
  puts "\e[33mThe document has been generated and is open in an other window\e[0m"
  puts ""
  puts ""
end

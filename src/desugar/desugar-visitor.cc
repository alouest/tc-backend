/**
 ** \file desugar/desugar-visitor.cc
 ** \brief Implementation of desugar::DesugarVisitor.
 */

#include <ast/all.hh>
#include <ast/libast.hh>
#include <desugar/desugar-visitor.hh>
#include <misc/algorithm.hh>
#include <misc/symbol.hh>
#include <parse/libparse.hh>
#include <parse/tweast.hh>

namespace desugar
{

  DesugarVisitor::DesugarVisitor(bool desugar_for_p,
                                 bool desugar_string_cmp_p)
    : super_type()
    , desugar_for_p_(desugar_for_p)
    , desugar_string_cmp_p_(desugar_string_cmp_p)
  {}


  /*-----------------------------.
  | Desugar string comparisons.  |
  `-----------------------------*/
  void
  DesugarVisitor::operator()(const ast::OpExp& e)
  {
    if (e.left_get().type_get() == &::type::String::instance()
	&& e.right_get().type_get() == &::type::String::instance())
      {
	if (e.oper_get() == ::ast::OpExp::Oper::eq)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ")"));
	else if (e.oper_get() == ::ast::OpExp::Oper::lt)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ") < 0"));
	else if (e.oper_get() == ::ast::OpExp::Oper::lt)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ") < 0"));
	else if (e.oper_get() == ::ast::OpExp::Oper::ne)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ") <> 0"));
	else if (e.oper_get() == ::ast::OpExp::Oper::gt)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ") > 0"));
	else if (e.oper_get() == ::ast::OpExp::Oper::ge)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ") >= 0"));
	else if (e.oper_get() == ::ast::OpExp::Oper::le)
	  result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
				       "streq (" << e.right_get() <<
				       ", " << e.left_get() << ") <= 0"));
      }
    else
      {
	Cloner::operator()(e);
      }
  // FIXedME: Some code was deleted here.
  }


  /*----------------------.
  | Desugar `for' loops.  |
  `----------------------*/

  /*<<-
    Desugar `for' loops as `while' loops:

       for i := lo to hi do
         body

     is transformed as:

       let
         var _lo := lo
         var _hi := hi
         var i := _lo
       in
         if i <= _hi then
           while 1 do
             (
               body;
               if i = _hi then
                 break;
               i := i + 1
             )
       end

     Notice that:

     - a `_hi' variable is introduced so that `hi' is evaluated only
       once;

     - a `_lo' variable is introduced to prevent `i' from being in the
       scope of `_hi';

     - a first test is performed before entering the loop, so that the
       loop condition becomes `i < _hi' (instead of `i <= _hi'); this
       is done to prevent overflows on INT_MAX.
       ->>*/

  void
  DesugarVisitor::operator()(const ast::ForExp& e)
  {
    const ast::Exp* init = e.vardec_get().init_get();
    result_ = std::get<ast::Exp*>(::parse::parse(::parse::Tweast() <<
			   "let \n var _lo := " << recurse(init) <<
 			   " \n var _hi := " << recurse(e.hi_get()) << "\n var " <<
			   e.vardec_get().name_get() << " := " <<
 			   recurse(init) << "\nin \n  if " <<
			   e.vardec_get().name_get() <<
			   " <= _hi then \n   while 1 do (\n    "
			   << recurse(e.body_get()) << ";\n   if " <<
			   e.vardec_get().name_get() << " = _hi then break;" <<
   			   e.vardec_get().name_get() << " := "  <<
			   e.vardec_get().name_get() << " + 1 ) end"));
    // FIXME: Some code was deleted here.
  }

} // namespace desugar

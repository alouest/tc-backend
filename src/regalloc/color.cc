/**
 ** \file regalloc/color.cc
 ** \brief Implementation for regalloc/color.hh.
 **/

#include <algorithm>
#include <climits>
#include <iostream>
#include <sstream>

#include <misc/algorithm.hh>
#include <regalloc/color.hh>
#include <target/cpu.hh>
#include <temp/temp.hh>

/// Run the 0-ary function \a What, verbosely.
#define DO(What)                                \
  do {                                          \
    if (trace_p_)                               \
      std::cerr << "## " #What ": begin.\n";    \
    timer_.push("9: " #What);                   \
    What();                                     \
    timer_.pop("9: " #What);                    \
    if (trace_p_)                               \
      std::cerr << "## " #What ": end.\n";      \
  } while (false)


namespace regalloc
{

  Color::Color(assem::ProcFrag& frag, const target::Cpu& cpu,
               const temp::TempMap& tempmap,
               bool coalesce_p, bool trace_p)
    : ig_(InterferenceGraph(frag.file_name(),
                            frag.instrs_get(),
                            tempmap,
                            trace_p))
    , cpu_(cpu)
    , nb_regs_(cpu_.nb_regs())
    , asm_proc_frag_(frag)
    , trace_p_(trace_p)
    , coalesce_p_(coalesce_p)
    , degree_(boost::num_vertices(ig_))
    , move_list_(boost::num_vertices(ig_))
    , alias_(boost::num_vertices(ig_))
    , color_(boost::num_vertices(ig_))
  {
    timer_ << ig_.timer_get();
    // Pre-treatments.
    DO(precolor);
    DO(import_nodes);
    if (coalesce_p)
      DO(import_moves);
    DO(degrees_compute);
    DO(priorities_compute);
    DO(make_work_list);
  }

  void
  Color::import_nodes()
  {
    for (auto [i, end] = boost::vertices(ig_); i != end; ++i)
      if (!(*i % precolored_))
        initial_ += *i;
  }

  void
  Color::import_moves()
  {
    work_list_moves_ = ig_.moves_get();
    for (const move_type& m : work_list_moves_)
      {
        move_list_[m.first].insert(m);
        move_list_[m.second].insert(m);
      }
  }

  void
  Color::precolor()
  {
  // FIXedME: Some code was deleted here.
    for (auto r : cpu_.registers_get())
    {
      if (!ig_.has(r))
      	continue;
      precolored_.insert(ig_.node_of(r));
      color_[ig_.node_of(r)] = r;
    }
    //maybe more (?)
  }

  void
  Color::priorities_compute()
  {
  // FIXedME: Some code was deleted here.
    assem::Instrs& instrs = asm_proc_frag_.instrs_get();
    for (auto instr : instrs)
    {
      for (temp::Temp def : instr->def())
      {
        auto present = temps_priority_.find(def);
        if (present == temps_priority_.end())
          temps_priority_.insert_or_assign(def, 1);
        else
          temps_priority_.insert_or_assign(present->first, present->second + 1);
      }
      for (temp::Temp use : instr->use())
      {
        auto present = temps_priority_.find(use);
        if (present == temps_priority_.end())
          temps_priority_.insert_or_assign(use, 1);
        else
          temps_priority_.insert_or_assign(present->first, present->second + 1);
      }
    }
  }

  void
  Color::degrees_compute()
  {
    for (auto [v, end] = boost::vertices(ig_); v != end; v++)
    {
      size_t i = 0;
      for(auto [first, last] = boost::adjacent_vertices(*v, ig_); first != last; first++)
	i++;
      degree_.emplace_back(i);
      
    }
    
  // FIXedME: Some code was deleted here.
    
  }

  void
  Color::add_edge(const node_type u, const node_type v)
  {
    if (!adjacent(u, v) && (u != v))
    {
      if (precolored_.find(u) == precolored_.end())
      {
        boost::add_edge(u, v, ig_);
        degree_[u]++;
      }
      if (precolored_.find(v) == precolored_.end())
      {
        boost::add_edge(v, u, ig_);
        degree_[v]++;
      }
    }
  // FIXedME: Some code was deleted here.
  }

  void
  Color::make_work_list(const node_type n)
  {
    if (getenv("DEBUG"))
      std::cout << "make_work_list(arg)" << std::endl;

    if (degree_[n] >= precolored_.size())
      spill_work_list_.insert(n);
    else if (move_related(n))
      freeze_work_list_.insert(n);
    else
      simplify_work_list_.insert(n);
    initial_ -= n;
  // FIXedME: Some code was deleted here.
  }

  void
  Color::make_work_list()
  {
    if (getenv("DEBUG"))
      std::cout << "make_work_list" << std::endl;
    while(!initial_.empty())
    {
      make_work_list(*initial_.begin());
    }
  // FIXedME: Some code was deleted here.
  }


  Color::node_set_type
  Color::adjacent(const node_type n) const
  {
    if (getenv("DEBUG"))
      std::cout << "adjacent" << std::endl;
    Color::node_set_type x;
    auto adj = boost::adjacent_vertices(n, ig_);
    for (auto it = adj.first; it != adj.second; ++it)
      x.insert(*it);
    for (auto i : select_stack_)
    {
      x.erase(i);
    }
    for (auto i : coalesced_nodes_)
    {
      x.erase(i);
    }
    return x;
  // FIXedME: Some code was deleted here.
  }

  bool
  Color::adjacent(const node_type a, const node_type b) const
  {
    if (getenv("DEBUG"))
      std::cout << "adjacent" << std::endl;
    auto adj = boost::adjacent_vertices(a, ig_);
    for (auto it = adj.first; it != adj.second; ++it)
      if (*it == b)
        return true;
    return false;
  // FIXedME: Some code was deleted here.
  }

  Color::move_set_type
  Color::node_moves(const node_type n) const
  {
    if (getenv("DEBUG"))
      std::cout << "node_moves" << std::endl;
    move_set_type l;
    move_set_type l2;
    for (auto i : active_moves_)
    {
      l2.insert(i);
    }
    for (auto i = work_list_moves_.begin(); i != work_list_moves_.end(); i++)
    {
      l2.insert(*i);
    }
    for (auto i : move_list_[n])
    {
      if (l2.find(i) != l2.end())
        l.insert(i);
    }
    return l;
  // FIXedME: Some code was deleted here.
  }


  bool
  Color::move_related(const node_type n) const
  {
    if (getenv("DEBUG"))
      std::cout << "move_related" << std::endl;    
    return node_moves(n).size() != 0;
  // FIXedME: Some code was deleted here.
  }


  bool
  Color::coalesce_briggs_p(const node_type a, const node_type b) const
  {
    size_t k = 0;
    auto adj = boost::adjacent_vertices(a, ig_);
    for (auto it = adj.first; it != adj.second; ++it)
    {
      if (degree_[*it] >= precolored_.size())
        k++;
    }
    adj = boost::adjacent_vertices(b, ig_);
    for (auto it = adj.first; it != adj.second; ++it)
    {
      if (degree_[*it] >= precolored_.size())
        k++;
    }
    return k < precolored_.size();
  // FIXedME: Some code was deleted here.
  }

  bool
  Color::coalesce_george_p(const node_type a, const node_type b) const
  {
    return degree_[a] < precolored_.size() ||
        precolored_.find(a) != precolored_.end() ||
        adjacent(a, b);
  // FIXedME: Some code was deleted here.
  }

  bool
  Color::all_adj_ok(std::pair<Color::node_type, Color::node_type> pair)
  {
    for (auto t : adjacent(pair.second))
      if (!coalesce_george_p(t, pair.first))
        return false;
    return true;
  }

  void
  Color::coalesce()
  {
  // FIXedME: Some code was deleted here.
    auto move = *(work_list_moves_.begin());
    auto x = get_alias(move.first);
    auto y = get_alias(move.second);
    auto pair = std::make_pair(x, y);
    if (precolored_.find(y) != precolored_.end())
      pair = std::make_pair(y, x);
    work_list_moves_.erase(work_list_moves_.find(move));
    auto u_in_precolored = precolored_.find(pair.first) != precolored_.end();
    if (pair.first == pair.second)
    {
      coalesced_moves_.insert(move);
      add_work_list(pair.first);
    }
    else if (precolored_.find(pair.second) != precolored_.end() ||
             adjacent(pair.first, pair.second))
    {
      constrained_moves_.insert(move);
      add_work_list(pair.first);
      add_work_list(pair.second);
    }
    else if ((u_in_precolored && all_adj_ok(pair)) ||
        (!u_in_precolored && coalesce_briggs_p(pair.first, pair.second)))
    {
      coalesced_moves_.insert(move);
      combine(pair.first, pair.second);
      add_work_list(pair.first);
    }
    else
      active_moves_.insert(move);
  }


  void
  Color::add_work_list(const node_type u)
  {
    if (getenv("DEBUG"))
      std::cout << "add_work_list" << std::endl;

    if (precolored_.find(u) == precolored_.end() && !move_related(u) &&
        degree_[u] < precolored_.size())
    {
      freeze_work_list_.erase(u);
      simplify_work_list_.insert(u);
    }
  // FIXedME: Some code was deleted here.
  }


  Color::node_type
  Color::get_alias(const node_type a) const
  {
    if (coalesced_nodes_.find(a) != coalesced_nodes_.end())
      return get_alias(alias_[a]);
    else
      return a;
  // FIXedME: Some code was deleted here.
  }

  void
  Color::combine(const node_type u, const node_type v)
  {
  // FIXedME: Some code was deleted here.
    auto it_v = freeze_work_list_.find(v);
    if (it_v != freeze_work_list_.end())
      freeze_work_list_.erase(it_v);
    else
      spill_work_list_.erase(spill_work_list_.find(v));
    coalesced_nodes_.insert(v);
    alias_.at(v) = u;
    move_list_[u].merge( move_list_[v]);
    enable_moves(v);
    for (auto t : adjacent(v))
    {
      if (getenv("DEBUG"))
	std::cout << "call decrement_degree from combine" << std::endl;

      add_edge(t, u);
      decrement_degree(t);
    }
    auto it_u = freeze_work_list_.find(u);
    if (degree_.at(u) >= precolored_.size() && it_u != freeze_work_list_.end())
    {
      freeze_work_list_.erase(it_u);
      spill_work_list_.insert(u);
    }
  }


  void
  Color::freeze_moves(const node_type u)
  {
  // FIXedME: Some code was deleted here.
    auto moves = node_moves(u);
    for (auto move = moves.begin(); move != moves.end(); move++)
    {
      if (getenv("DEBUG"))
	std::cout << "call node_moves from freeze_moves" << std::endl;
      auto v = get_alias(move->first);
      if (get_alias(move->second) != get_alias(u))
        v = get_alias(move->second);
      active_moves_.erase(active_moves_.find(*move));
      frozen_moves_.insert(*move);
      if (!node_moves(v).size() && degree_.at(v) < precolored_.size())
      {
        freeze_work_list_.erase(freeze_work_list_.find(v));
        simplify_work_list_.insert(v);
      }
    }
  }

  void
  Color::freeze()
  {
  // FIXedME: Some code was deleted here.
    auto it = freeze_work_list_.begin();
    simplify_work_list_.insert(*it);
    freeze_moves(*it);
    freeze_work_list_.erase(it);
  }

  // To compute the node with the least priority, avoid float
  // computations which are unsafe, and, in some extent, unstable.
  // Consider for instance the following code, that was designed as a
  // stripped version of a nondeterministic behavior of a previous
  // version of this very function (select_spill).  (You'll have to
  // add the backslashes to the macro by hand.)
  //
  // /tmp % cat foo.c
#if 0
     #include <stdio.h>

     #define TEST(Op)
       if ((num / den) Op quot)
         fprintf(stderr, "1 / 3 " #Op " 1 / 3\n");

     int
     main()
     {
       float quot = 1.0 / 3.0;
       volatile float num = 1.0;
       volatile float den = 3.0;

       TEST(<);
       TEST(<=);
       TEST(>);
       TEST(>=);
       TEST(==);
       TEST(!=);
     }
#endif
  // /tmp % gcc-3.4 foo.c -o foo-c
  // /tmp % ./foo-c
  // 1 / 3 < 1 / 3
  // 1 / 3 <= 1 / 3
  // 1 / 3 != 1 / 3

  Color::node_type
  Color::select_node()
  {
    long unsigned int val_max = 0;
    auto node_max = *spill_work_list_.begin();
    for (auto it = spill_work_list_.begin(); it != spill_work_list_.end(); ++it)
    {
      auto val = temps_priority_.find(ig_[*it])->second / degree_.at(*it);
      if (val > val_max)
      {
        val_max = val;
        node_max = *it;
      }
    }
    return node_max;
  }

  void
  Color::select_spill()
  {
  // FIXedME: Some code was deleted here.
    auto node = select_node();
    spill_work_list_.erase(spill_work_list_.find(node));
    simplify_work_list_.insert(node);
    freeze_moves(node);
  }


  void
  Color::simplify()
  {
    if (getenv("DEBUG"))
      std::cout << "simplify" << std::endl;

  // FIXedME: Some code was deleted here.
    if (!simplify_work_list_.size())
      return;
    auto node = *simplify_work_list_.begin();
    simplify_work_list_ -= node;
    select_stack_.insert(select_stack_.begin(), node);
    auto adj = boost::adjacent_vertices(node, ig_);
    for (auto it = adj.first; it != adj.second; ++it) {
      decrement_degree(*it);
    }
    
  }


  void
  Color::decrement_degree(node_type n)
  {
    if (getenv("DEBUG"))
      std::cout << "decrement_degree" << std::endl;

    auto d = degree_[n];
    degree_[n] = d - 1;
    if (d == precolored_.size())
    {
      node_set_type s;
      s.insert(n);
      auto adj = boost::adjacent_vertices(n, ig_);
      for (auto it = adj.first; it != adj.second; ++it) 
        s.insert(*it);
      if (move_related(n))
	freeze_work_list_.insert(n);
      else
	simplify_work_list_.insert(n);
      spill_work_list_.erase(n);
    }
    if (getenv("DEBUG"))
      std::cout << "decrement_degree out" << std::endl;
	
  // FIXedME: Some code was deleted here.
  }


  void
  Color::enable_moves(node_type n)
  {
    for (auto m : node_moves(n))
    {
      if (getenv("DEBUG"))
	std::cout << "call node_moves from enable_moves(arg)" << std::endl;
      if (active_moves_.find(m) != active_moves_.end())
      {
        active_moves_.erase(m);
        work_list_moves_.insert(m);
      }
    }
  // FIXedME: Some code was deleted here.
  }

  void
  Color::enable_moves(const node_set_type& nodes)
  {
    for (auto n : nodes)
    {
      for (auto m : node_moves(n))
      {
	if (getenv("DEBUG"))
	  std::cout << "call node_moves from enable_moves" << std::endl;
        if (active_moves_.find(m) != active_moves_.end())
        {
          active_moves_.erase(m);
          work_list_moves_.insert(m);
        }
      }
    }
  // FIXedME: Some code was deleted here.
  }


  void
  Color::assign_color(node_type n)
  {
  // FIXedME: Some code was deleted here.
    std::set<temp::Temp> ok_color;
    for (auto r : cpu_.registers_get())
      ok_color.insert(r);
    auto adj = boost::adjacent_vertices(n, ig_);
    for (auto it = adj.first; it != adj.second; ++it)
    {
      auto alias = get_alias(*it);
      if (colored_nodes_.find(alias) != colored_nodes_.end() ||
          precolored_.find(alias) != precolored_.end())
        ok_color.erase(color_.at(alias));
    }
    if (!ok_color.size())
      spilled_nodes_.insert(spilled_nodes_.begin(), n);
    else
    {
      colored_nodes_.insert(colored_nodes_.begin(), n);

      auto c = ok_color.begin();
      color_.at(n) = *c;
    }
    for (auto node : coalesced_nodes_)
      color_.at(node) = color_.at(get_alias(node));
  }

  void
  Color::assign_colors()
  {
  // FIXedME: Some code was deleted here.
   while (select_stack_.size())
    {
      auto node = select_stack_.begin();
      std::set<temp::Temp> ok_color_arg;
      std::set<temp::Temp> ok_color_specs;
      std::set<temp::Temp> ok_color_callee;
      std::set<temp::Temp> ok_color_caller;
      
      for (auto r : cpu_.argument_regs())
        ok_color_arg.insert(r);
      for (auto r : cpu_.special_regs())
        ok_color_specs.insert(r);
      for (auto r : cpu_.callee_save_regs())
        ok_color_callee.insert(r);
      for (auto r : cpu_.caller_save_regs())
        ok_color_caller.insert(r);

      
      auto adj = boost::adjacent_vertices(*node, ig_);
      for (auto it = adj.first; it != adj.second; ++it)
      {
        auto alias = get_alias(*it);
        if (colored_nodes_.find(alias) != colored_nodes_.end() ||
            precolored_.find(alias) != precolored_.end()) {
	  ok_color_arg.erase(color_.at(alias));
	  ok_color_caller.erase(color_.at(alias));
	  ok_color_callee.erase(color_.at(alias));
	  ok_color_specs.erase(color_.at(alias));
	}
      }

      if (!ok_color_caller.empty()){
	colored_nodes_.insert(colored_nodes_.begin(), *node);
	auto c = ok_color_caller.begin();
	color_.at(*node) = *c;
      } else if (!ok_color_callee.empty()) {
	colored_nodes_.insert(colored_nodes_.begin(), *node);
	auto c = ok_color_callee.begin();
	color_.at(*node) = *c;
      } else if (!ok_color_arg.empty()) {
	colored_nodes_.insert(colored_nodes_.begin(), *node);
	auto c = ok_color_arg.begin();
	color_.at(*node) = *c;
      } else if (!ok_color_specs.empty()) {
	colored_nodes_.insert(colored_nodes_.begin(), *node);
	auto c = ok_color_specs.begin();
	color_.at(*node) = *c;
      } else {
	spilled_nodes_.insert(spilled_nodes_.begin(), *node);
      }
      select_stack_.erase(node);
    }
   for (auto node : coalesced_nodes_)
     color_.at(node) = color_.at(get_alias(node));
  
  }

  bool
  Color::operator()()
  {
    if (trace_p_)
      std::cerr << "## color: begin.\n";
    do {
        if (trace_p_)
          dump(std::cerr);
        if (!simplify_work_list_.empty())
          DO(simplify);
        else if (!work_list_moves_.empty())
          DO(coalesce);
        else if (!freeze_work_list_.empty())
          DO(freeze);
        else if (!spill_work_list_.empty())
          DO(select_spill);
      } while (!simplify_work_list_.empty()
               || !work_list_moves_.empty()
               || !freeze_work_list_.empty()
               || !spill_work_list_.empty());

    // Pop the entire stack, assigning colors.
    DO(assign_colors);
    if (trace_p_)
      std::cerr << "## color: end.\n";

    return spilled_nodes_.empty();
  }


  const temp::temp_set_type
  Color::spilled_get() const
  {
    temp::temp_set_type res;
    for (const node_type& n : spilled_nodes_)
      res.insert(ig_[n]);
    return res;
  }

  temp::TempMap
  Color::tempmap_get() const
  {
    temp::TempMap res;
    for (auto [v, end] = boost::vertices(ig_); v != end; v++)
    {
      if (precolored_.find(*v) == precolored_.end())
	res.insert(std::pair(ig_[*v], color_.at(*v)));
    }
    // FIXedME: Some code was deleted here.
    return res;
  }

  const misc::timer&
  Color::timer_get() const
  {
    return timer_;
  }

  void
  Color::trace(const std::string& what, const node_type& n) const
  {
    if (trace_p_)
      {
        dump(std::cerr, n);
        std::cerr << '\t' << what << '\n';
      }
  }

  void
  Color::trace(const std::string& what, const move_type& m) const
  {
    if (trace_p_)
      {
        dump(std::cerr, m);
        std::cerr << '\t' << what << '\n';
      }
  }

  void
  Color::dump(std::ostream& ostr, const node_type& n) const
  {
    // ostr << n << " (" << *ig_[n] << ")";
    ostr << ig_[n]; //  << " (" << degree_[n] << ")";
    if (n % colored_nodes_)
      std::cerr << " (" << color_[n] << ')';
  }

  void
  Color::dump(std::ostream& ostr, const node_set_type& s) const
  {
    for (node_set_type::const_iterator i = s.begin(); i != s.end(); ++i)
      {
        if (i != s.begin())
          ostr << " ";
        dump(ostr, *i);
      }
  }

  void
  Color::dump(std::ostream& ostr, const node_list_type& s) const
  {
    for (node_list_type::const_iterator i = s.begin(); i != s.end(); ++i)
      {
        if (i != s.begin())
          ostr << " ";
        dump(ostr, *i);
      }
  }

  void
  Color::dump(std::ostream& ostr, const move_type& s) const
  {
    dump(ostr, s.first);
    ostr << ":=";
    dump(ostr, s.second);
  }

  void
  Color::dump(std::ostream& ostr, const move_set_type& s) const
  {
    for (move_set_type::const_iterator i = s.begin(); i != s.end(); ++i)
      {
        if (i != s.begin())
          ostr << " ";
        dump(ostr, *i);
      }
  }

  void
  Color::dump(std::ostream& ostr) const
  {
#   define REPORT(Title, What)                          \
    do {                                                \
      ostr << Title << " (" << (What).size() << "): ";  \
      dump(ostr, What);                                 \
      ostr << '\n';                                     \
    } while (0)

    size_t size = 0;
    size += precolored_.size();
    size += initial_.size();
    size += simplify_work_list_.size();
    size += freeze_work_list_.size();
    size += spill_work_list_.size();
    size += spilled_nodes_.size();
    size += coalesced_nodes_.size();
    size += colored_nodes_.size();
    size += select_stack_.size();

    ostr << "===================="
         << " Nodes " << ig_.name_get() << " (" << size << ") "
         << "====================\n";

    REPORT("Precolor   ", precolored_);
    REPORT("Initial    ", initial_);
    REPORT("Simplify WL", simplify_work_list_);
    REPORT("Freeze WL  ", freeze_work_list_);
    REPORT("Spill WL   ", spill_work_list_);
    REPORT("Spilled    ", spilled_nodes_);
    REPORT("Coalesced  ", coalesced_nodes_);
    REPORT("Colored    ", colored_nodes_);
    REPORT("Select     ", select_stack_);

    if (coalesce_p_)
      {
        size = 0;
        size += coalesced_moves_.size();
        size += constrained_moves_.size();
        size += frozen_moves_.size();
        size += work_list_moves_.size();
        size += active_moves_.size();

        ostr << "--------------------"
             << " Moves " << ig_.name_get() << " (" << size << ") "
             << "--------------------\n";

        REPORT("Coalesced  ", coalesced_moves_);
        REPORT("Constrained", constrained_moves_);
        REPORT("Frozen     ", frozen_moves_);
        REPORT("Moves WL   ", work_list_moves_);
        REPORT("Active     ", active_moves_);
      }
#   undef REPORT
  }

} // namespace regalloc


# undef DO

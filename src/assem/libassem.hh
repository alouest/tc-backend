/**
 ** \file assem/libassem.hh
 ** \brief Assem module interface.
 */

#pragma once

#include <assem/comment.hh>
#include <assem/fragment.hh>
#include <assem/fragments.hh>
#include <assem/instr.hh>
#include <assem/label.hh>
#include <assem/move.hh>
#include <assem/oper.hh>

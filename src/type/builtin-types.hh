/**
 ** \file type/builtin-types.hh
 ** \brief The classes Void, Int, String.
 */
#pragma once

#include <type/fwd.hh>
#include <type/type.hh>

namespace type
{
  template <typename T>
  class Singleton
  {
  public:
    // Make it non-copyable
    Singleton(const Singleton<T>&) = delete;
    Singleton(Singleton<T>&&) = delete;
    Singleton& operator=(const Singleton<T>&&) = delete;
    Singleton& operator=(Singleton<T>&&) = delete;

    // Return the unique instance of this class
    static const T& instance()
    {
      static T instance;
      return instance;
    }

    //  private:
    Singleton() = default;
  };

  class Int : public Singleton<Int>, public Type
  {
    void accept(ConstVisitor& v) const;
    void accept(Visitor& v) override;
  };

  class String : public Singleton<String>, public Type
  {
    void accept(ConstVisitor& v) const;
    void accept(Visitor& v) override;
  };

  class Void : public Singleton<Void>, public Type
  {
    void accept(ConstVisitor& v) const;
    void accept(Visitor& v) override;
  };
  // FIXedME: Some code was deleted here (Other types : String, Int, Void).
} // namespace type

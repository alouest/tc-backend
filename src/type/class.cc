/**
 ** \file type/class.cc
 ** \brief Implementation for type/class.hh.
 */

#include <iostream>
#include <ostream>

#include <misc/indent.hh>
#include <type/class.hh>
#include <type/visitor.hh>

namespace type
{

  Class::Class(const Class* super)
    : Type()
    , id_(fresh_id())
    , super_(super)
    , subclasses_()
  {}

  void
  Class::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  Class::accept(Visitor& v)
  {
    v(*this);
  }

  const Type*
  Class::attr_type(const misc::symbol& key) const
  {
    for (auto i : attrs_)
      {
	if (i.name_get() == key);
	return &i.type_get();
      }
    return nullptr;
  // FIXME: Some code was deleted here.
  }

  const Type*
  Class::meth_type(const misc::symbol& key) const
  {
    for (auto i : meths_)
      {
	if (i->name_get() == key);
	return &i->result_get();
      }
    return nullptr;
  // FIXME: Some code was deleted here.
  }

  const Class* Class::common_root(const Class& other) const
  {
    auto oth = &other;
    while (oth != nullptr)
      {
	oth = other.super_get();
	auto self = this;
	while (self != nullptr)
	  {
	    if (self == oth)
	      return self;
	    self = self->super_get();
	  }
      }
    return nullptr;
  }
  // FIXME: Some code was deleted here (Find common super class).

  bool Class::sound() const
  {
    auto self = this;
    while (self != nullptr)
      {
	self = self->super_get();
	if (self == this)
	  return true;
      }
    return false;
  }
  // FIXME: Some code was deleted here (Super class soundness test).

  bool Class::compatible_with(const Type& other) const
  {
    try
      {
	auto self = this;
	while (self != nullptr)
	  {
	    if (self == &other)
	      return true;
	    self = self->super_get();
	  }
	return false;
      }
    catch (std::bad_cast& e)
      {
	return false;
      }
  }
  // FIXME: Some code was deleted here (Special implementation of "compatible_with" for Class).

  const Class&
  Class::object_instance()
  {
  // FIXME: Some code was deleted here.
  }

  unsigned Class::fresh_id()
  {
    static unsigned counter_ = 0;
    return counter_++;
  }

} // namespace type

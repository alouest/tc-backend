/**
 ** \file type/array.hxx
 ** \brief Inline methods for type::Array.
 */
#pragma once

#include <misc/contract.hh>
#include <type/array.hh>

namespace type
{
  inline
  bool Array::compatible_with(const Type& other) const
  {
    const Array* child = dynamic_cast<const Array*>(&other);
    if (child)
      return child == this;
    else
      return other.compatible_with(*this);
  }
  // FIXME: Some code was deleted here.

} // namespace type

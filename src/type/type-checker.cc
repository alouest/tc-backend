/**
 ** \file type/type-checker.cc
 ** \brief Implementation for type/type-checker.hh.
 */

#include <memory>

#include <ast/all.hh>
#include <type/type-checker.hh>
#include <type/types.hh>

namespace type
{

  namespace
  {

    // Try to cast the type into a nil type.
    // If it's not actually a `type::Nil`, return `nullptr`.
    // This allows us to write more clear code like:
    // if (auto nil = to_nil(e.type_get())
    // ...
    const Nil* to_nil(const Type& type)
    {
      return dynamic_cast<const Nil*>(&type.actual());
    }

  }

  TypeChecker::TypeChecker()
    : super_type()
    , error_()
  {}

  const Type*
  TypeChecker::type(ast::Typable& e)
  {
    if (!e.type_get())
      e.accept(*this);
    return e.type_get();
  // FIXME: Some code was deleted here.
  }

  const Record*
  TypeChecker::type(const ast::fields_type& e)
  {
    auto res = new Record;
    for (auto i: e)
      for (auto j: e)
        if (i->name_get() == j->name_get() && i != j)
          error_ << misc::error::error_type::type << j->location_get()
                 << " duplicate fields" << std::endl;
    for (const auto& i : e)
    {
      res->field_add(i->name_get(), *type(i->type_name_get()));
    }
    // FIXME: Some code was deleted here.
    return res;
  }

  const Record*
  TypeChecker::type(const ast::VarDecs& e)
  {
    auto res = new Record;
    for (const auto& i : e.decs_get())
      res->field_add(i->name_get(), *type(*i));

    return res;
  }

  const misc::error&
  TypeChecker::error_get() const
  {
    return error_;
  }


  /*-----------------.
  | Error handling.  |
  `-----------------*/

  void
  TypeChecker::error(const ast::Ast& ast, const std::string& msg)
  {
    error_ << misc::error::error_type::type
           << ast.location_get() << ": " << msg << std::endl;
  }

  void
  TypeChecker::type_mismatch(const ast::Ast& ast,
                             const std::string& exp1, const Type& type1,
                             const std::string& exp2, const Type& type2)
  {
    error_ << misc::error::error_type::type
           << ast.location_get() << ": type mismatch" << misc::incendl
           << exp1 << " type: " << type1 << misc::iendl
           << exp2 << " type: " << type2 << misc::decendl;
  }

  void
  TypeChecker::check_types(const ast::Ast& ast,
                           const std::string& exp1, const Type& type1,
                           const std::string& exp2, const Type& type2)
  {
    if (!type1.compatible_with(type2))
    {
      type_mismatch(ast, exp1, type1, exp2, type2);
    }
  // FIXME: Some code was deleted here.
  }


  void
  TypeChecker::check_types(const ast::Ast& ast,
                           const std::string& exp1, ast::Typable& type1,
                           const std::string& exp2, ast::Typable& type2)
  {
    // Ensure evaluation order.
    const Type& one = *type(type1);
    const Type& two = *type(type2);
    if (!one.compatible_with(two))
    {
      type_mismatch(ast, exp1, one, exp2, two);
    }
  // FIXME: Some code was deleted here (Check types).
  }


  /*--------------------------.
  | The core of the visitor.  |
  `--------------------------*/

  /*-----------------.
  | Visiting /Var/.  |
  `-----------------*/

  void
  TypeChecker::operator()(ast::SimpleVar& e)
  {
    e.type_set(e.def_get()->type_get());
  // FIXME: Some code was deleted here.
  }

  void
  TypeChecker::operator()(ast::CastVar& e)
  {
    e.type_set(e.ty_get().type_get());
  }

  void
  TypeChecker::operator()(ast::FieldVar& e)
  {
    type(e.var_get());
    const Record* rec;
    auto nam = dynamic_cast<const Named*>(e.var_get().type_get());
    if (nam)
      rec = dynamic_cast<const Record*>(&nam->actual());
    else
      rec = dynamic_cast<const Record*>(e.var_get().type_get());
    if (rec)
      {
      int exist = 0;
      for (auto i : rec->fields_get())
        {
          if (i.name_get() == e.name_get())
            {
              exist++;
              auto nam2 = dynamic_cast<const Named*>(&i.type_get());
              if (nam2)
                e.type_set(&nam2->actual());
              else
                e.type_set(&i.type_get());
            }
        }
      if (exist == 0)
        {
          error_ << misc::error::error_type::type << e.location_get()
                 << " invalid field: " << e.name_get() << std::endl;
          e.type_set(&Void::instance());
        }
      }
    else {
      error_ << misc::error::error_type::type << e.location_get()
             << " invalid field: " << e.name_get() << std::endl;
      e.type_set(&Void::instance());
    }
  }

  void
  TypeChecker::operator()(ast::SubscriptVar& e)
  {
    const Array* arr = dynamic_cast<const Array*>(type(e.var_get()));
    if (!arr)
      {
        const Named* nam = dynamic_cast<const Named*>(e.var_get().type_get());
        if (nam)
          arr = dynamic_cast<const Array*>(&nam->actual());
        else
          {
            error_ << misc::error::error_type::type << e.location_get()
                   << " Variable not an array." << std::endl;
            e.type_set(&Void::instance());
          }
      }
  
    if (arr)
      e.type_set(&arr->elt_get());
  }

  // FIXME: Some code was deleted here.

  /*-----------------.
  | Visiting /Exp/.  |
  `-----------------*/

  // Literals.
  void
  TypeChecker::operator()(ast::NilExp& e)
  {
    auto nil_ptr = std::make_unique<Nil>();
    type_default(e, nil_ptr.get());
    created_type_default(e, nil_ptr.release());
  }

  void
  TypeChecker::operator()(ast::IntExp& e)
  {
    e.type_set(&Int::instance());
  // FIXME: Some code was deleted here.
  }

  void
  TypeChecker::operator()(ast::StringExp& e)
  {
    e.type_set(&String::instance());
  // FIXME: Some code was deleted here.
  }


  // Complex values.

  void
  TypeChecker::operator()(ast::RecordExp& e)
  {
    // If no error occured, check for nil types in the record initialization.
    // If any error occured, there's no need to set any nil types.
    // If there are any record initializations, set the `record_type_`
    // of the `Nil` to the expected type.
    auto nil = new Nil();
    const Named* nam = dynamic_cast<const Named*>
    (e.get_type().def_get()->created_type_get());
    if (nam != nullptr)
      {
        const Record* rec = dynamic_cast<const Record*>
          (&nam->actual());
        const auto& field_th = rec->fields_get();
        std::vector<::ast::FieldInit*> field = e.get_fields();
        auto it = field.begin();
        if (rec != nullptr)
          {
            for (auto it_th = field_th.begin(); it_th != field_th.end(); it_th++)
              {
                if (it == field.end()) {
                  error_ << misc::error::error_type::type << e.location_get()
                         << " Reccord initialisation not complete." << std::endl;
                  break;
                }
                
                if ((*it)->name_get() != it_th->name_get())
                  error_ << misc::error::error_type::type << (*it)->location_get()
                         << " Missnamed field: " << std::endl << "Expected: "
                         << it_th->name_get() << std::endl << "Input: "
                         << (*it)->name_get();
                
                
                (*it)->accept(*this);
                auto nil_init = dynamic_cast<const Nil*>((*it)->init_get().type_get());
                if (nil_init != nullptr)
                  {
                    auto nam = dynamic_cast<const Named*>(&it_th->type_get());
                    nil_init->set_record_type(nam->actual());
                  }
                
                check_type((*it)->init_get(), (*it)->name_get(), it_th->type_get());
                //if (!it->type_get()->compatible_with(it_th->type_get()))
                
                it = it+1;
              }
          }
        if (it != field.end())
          error_ << misc::error::error_type::type << e.location_get()
                 << " Too many field in initialisation." << std::endl;
        e.type_set(e.get_type().def_get()->created_type_get());
      }
    if (!error_)
      e.get_type().accept(*this);
    else
      e.type_set(&Void::instance());
    // FIXME: Some code was deleted here.
  }
  
  void
  TypeChecker::operator()(ast::OpExp& e)
  {
  // FIXME: Some code was deleted here.

    // If any of the operands are of type Nil, set the `record_type_` to the
    // type of the opposite operand.
    if (!error_)
      {
  // FIXME: Some code was deleted here.
        e.left_get().accept(*this);
        e.right_get().accept(*this);
        if ((e.left_get().type_get() == nullptr || e.right_get().type_get() == nullptr) || (to_nil(*e.left_get().type_get()) != 0
            && (to_nil(*e.right_get().type_get()) != 0)))
          {
            error_ << misc::error::error_type::type << e.location_get()
                   << " Nil equal nil not valid" << std::endl;
            e.type_set(&Void::instance());
            return;
          }
        try
          {
            const auto& type_r =
              dynamic_cast<const Nil&>(*e.right_get().type_get());
            
            type_r.set_record_type(*e.left_get().type_get());
          }
        catch (std::bad_cast& e)
          {}
        try
          {
            const auto& type_l =
              dynamic_cast<const Nil&>(*e.left_get().type_get());
            type_l.set_record_type(*e.right_get().type_get());
          }
        catch (std::bad_cast& e)
          {}
        
        if (e.right_get().type_get()->compatible_with(*e.left_get().type_get()) ||
            e.left_get().type_get()->compatible_with(*e.right_get().type_get()))
          {
            e.type_set(&Int::instance());
          }
        else
          {
            type_mismatch(e, "Right operand", *e.right_get().type_get(),
                          "Left operand", *e.left_get().type_get());
          }
      }
    type_default(e, &Int::instance());
  }

  // FIXME: Some code was deleted here.
  void
  TypeChecker::operator()(ast::BreakExp& e)
  {
    e.type_set(&Void::instance());
  }

  void
  TypeChecker::operator()(ast::CallExp& e)
  {
    try
      {
        const Function* fun = dynamic_cast<const Function*>
          (e.def_get()->created_type_get());
        const auto& field_th = fun->formals_get();


        e.type_set(&fun->result_get());
        if (e.arg_get())
          {
            std::vector<::ast::Exp*> args = *e.arg_get();
            
            
            auto it = args.begin();
            for (auto it_th = field_th.begin(); it_th != field_th.end(); it_th++)
              {
                if (it == args.end())
                {
                  error_ << misc::error::error_type::type << e.location_get()
                         << " function call not complete." << std::endl;
                  break;
                }
                (*it)->type_set(type(*(*it)));
                auto nil_init = dynamic_cast<const Nil*>((*it)->type_get());
                if (nil_init != nullptr)
                  {
                    auto nam = dynamic_cast<const Named*>(&it_th->type_get());
                    nil_init->set_record_type(nam->actual());
                  }             
                check_type(*(*it), "", it_th->type_get());
                it = it + 1;
              }
            if (it != args.end())
              error_ << misc::error::error_type::type << e.location_get()
                     << "Invalid number in formals." << std::endl;
          }
            //do error(too many field)
        auto fun2 = dynamic_cast<const Function*>(e.def_get()->created_type_get());
        if (fun2)
          {
            auto nam = dynamic_cast<const Named*>(&fun2->result_get());
            if (nam)
              e.type_set(&nam->actual());
            else
              e.type_set(&fun2->result_get());
          }
      }
    catch (std::bad_cast& e)
      {}
    if (!error_)
      {}
  }

  void TypeChecker::operator()(ast::WhileExp& e)
  {
    type(e.test_get());
    type(e.body_get());
    
    check_type(e.test_get(), "", Int::instance());
    check_type(e.body_get(), "", Void::instance());
    e.type_set(&Void::instance());
  }

  void TypeChecker::operator()(ast::IfExp& e)
  {
    type(e.get_if());
    type(e.get_then());
    check_type(e.get_if(), "", Int::instance());
    if (e.get_else())
      {
        type(*e.get_else());
        const Nil* nil1 = dynamic_cast<const Nil*>(e.get_then().type_get());
        const Nil* nil2 = dynamic_cast<const Nil*>(e.get_else()->type_get());
        if (nil1 && !nil2)
          {
            const Named* nam = dynamic_cast<const Named*>(e.get_else()->type_get());
            if (nam)
              nil1->set_record_type(nam->actual());
            else
              nil1->set_record_type(*e.get_else()->type_get());
          }
        else if (nil2 && !nil1)
          {
            const Named* nam = dynamic_cast<const Named*>(e.get_then().type_get());
            if (nam)
              nil2->set_record_type(nam->actual());
            else
              nil2->set_record_type(*e.get_else()->type_get());
          }
        else
          check_type(e.get_then(), "", *e.get_else()->type_get());
      }
    else
      {
        check_type(e.get_then(), "", Void::instance());
      }
    e.type_set(e.get_then().type_get());
  }

  void TypeChecker::operator()(ast::LetExp& e)
  {
    for (auto i : e.get_decs().decs_get())
      i->accept(*this);
    type(e.get_exps());
    e.type_set(e.get_exps().type_get());
  }

  void TypeChecker::operator()(ast::SeqExp& e)
  {
    if (e.get_exps().size() != 0)
    {
      for (auto i : e.get_exps())
      {
        type(*i);
      }
      e.type_set((e.get_exps().back())->type_get());
    }
    else
    {
      e.type_set(&Void::instance());
    }
  }

  void TypeChecker::operator()(ast::ArrayExp& e)
  {
    auto ret = type(e.name_get());
    const Named* nam = dynamic_cast<const Named*>(ret);
    const Array* arr = dynamic_cast<const Array*>(&nam->actual());
    if (!arr)
      {
        std::cerr << "That shouldn't happend" << std::endl;
      }
    type(e.of_get());
    check_type(e.of_get(), "", arr->elt_get());
    e.type_set(arr);
  }

  void TypeChecker::operator()(ast::AssignExp& e)
  {
    auto var = type(e.var_get());
    auto val = type(e.val_get());
    const Nil* nil = dynamic_cast<const Nil*>(e.val_get().type_get());
    if (nil)
      {
        nil->set_record_type(*e.var_get().type_get());
        e.type_set(&Void::instance());
      }
    else
      {
        const ast::SimpleVar* sim = dynamic_cast<const ast::SimpleVar*>(&e.var_get());
        if (sim && var_read_only_.find(sim->def_get()) != var_read_only_.end())
          error_ << misc::error::error_type::type << e.var_get().location_get()
                 << ": Variable is read only" << std::endl;
        auto nam = dynamic_cast<const Named*>(var);
        if (nam)
          check_type(e.val_get(), "", nam->actual());
        else
          check_type(e.val_get(), "", *var);
        e.type_set(&Void::instance());
      }
  }

  void TypeChecker::operator()(ast::ForExp& e)
  {
    e.vardec_get().accept(*this);
    if (!Int::instance().compatible_with(*type(e.hi_get())))
      error_ << misc::error::error_type::type << e.location_get()
            << "Invalid  upper bound : \n" << "type: " << *e.hi_get().type_get()
            <<"\nExpected: Int \n";
    
    var_read_only_.insert(&e.vardec_get());
    type(e.body_get());
    check_type(e.body_get(), "", Void::instance());
    e.type_set(&Void::instance());
  }

  void TypeChecker::operator()(ast::CastExp& e)
  {
    e.exp_get().accept(*this);
    e.type_set(type(e.ty_get()));
  }
  /*-----------------.
  | Visiting /Dec/.  |
  `-----------------*/

  /*------------------------.
  | Visiting FunctionDecs.  |
  `------------------------*/

  void
  TypeChecker::operator()(ast::FunctionDecs& e)
  {
    decs_visit<ast::FunctionDec>(e);
  }


  void
  TypeChecker::operator()(ast::FunctionDec&)
  {
    // We must not be here.
    unreachable();
  }


  // Store the type of this function.
  template <>
  void
  TypeChecker::visit_dec_header<ast::FunctionDec>(ast::FunctionDec& e)
  {
    auto rec = new Record();
    for (auto i : e.formals_get().decs_get())
      {
        
        i->accept(*this);
        if (i->type_name_get()->name_get() == "string")
          {
            rec->field_add(i->name_get(), String::instance());
          }
        else if (i->type_name_get()->name_get() == "int")
          {
            rec->field_add(i->name_get(), Int::instance());
          }
        else if (i->type_name_get()->name_get() == "void")
          {
            rec->field_add(i->name_get(), Void::instance());
          }
        else
          rec->field_add(i->name_get(), *i->type_name_get()->def_get()->created_type_get());
      }
    
    if (e.result_get())
      {
        type(*e.result_get());
        if (e.result_get()->name_get() == "string")
          {
            e.created_type_set(new Function(rec, String::instance()));
          }
        else if (e.result_get()->name_get() == "int")
          {
            e.created_type_set(new Function(rec, Int::instance()));
          }
        else if (e.result_get()->name_get() == "void")
          {
            e.created_type_set(new Function(rec, Void::instance()));
          }
        else
          e.created_type_set(new Function(rec, *e.result_get()->type_get()));
      }
    else
      e.created_type_set(new Function(rec, Void::instance()));

    
    if (e.name_get() == "_main" && e.result_get() != nullptr && e.result_get()->type_get() != &Void::instance())
      error_ << misc::error::error_type::type <<  e.location_get()
             << " Invalid main type." << std::endl;
    if (e.name_get() == "_main" && e.formals_get().decs_get().size() != 0)
      error_ << misc::error::error_type::type <<  e.location_get()
             << " Bad main type arguments." << std::endl;
    // FIXME: Some code was deleted here.
  }


  // Type check this function's body.
  template <>
  void
  TypeChecker::visit_dec_body<ast::FunctionDec>(ast::FunctionDec& e)
  {
    if (e.body_get())
      {
        
        visit_routine_body<Function>(e);
        const Function* fun = dynamic_cast<const Function*>(e.created_type_get());
	const Nil* nil = dynamic_cast<const Nil*>(e.body_get()->type_get());
	if (nil && e.result_get())
	    nil->set_record_type(*e.result_get()->type_get());
        check_type(*e.body_get(), "", fun->result_get());
        // Check for Nil types in the function body.
        if (!error_)
          {
            if (auto i = to_nil(*(e.body_get()->type_get())))
            {
              i->set_record_type(*(e.result_get()->type_get()));
            }
          }
  // FIXME: Some code was deleted here.
      }
  }
  
  /*---------------.
  | Visit VarDec.  |
  `---------------*/

  void
  TypeChecker::operator()(ast::VarDec& e)
  {
    if (e.init_get())
      type(*e.init_get());

    if (!e.type_name_get())
      if (!e.init_get() && !to_nil(*(e.init_get()->type_get())))
      {
        //error
      }
      else
      {
        e.type_set(e.init_get()->type_get());
      }
    else
    {
      type(*(e.type_name_get()));
      if (e.init_get() && to_nil(*e.init_get()->type_get()))
        {
          auto nam = dynamic_cast<const Named*>(e.type_name_get()->def_get()->created_type_get());
          if (nam != nullptr)
            to_nil(*e.init_get()->type_get())->set_record_type(nam->actual());
          else
            to_nil(*e.init_get()->type_get())->set_record_type(*e.type_name_get()->def_get()->created_type_get());
        }
      if (e.init_get())
        if (e.type_name_get()->name_get() == "string")
            check_type(*e.init_get(), "", String::instance());
        else if (e.type_name_get()->name_get() == "int")
            check_type(*e.init_get(), "", Int::instance());
        else if (e.type_name_get()->name_get() == "void")
            check_type(*e.init_get(), "", Void::instance());
        else
          check_type(*e.init_get(), "", *(e.type_name_get()->def_get()->created_type_get()));
      
      if (!error_)
        {
          e.type_set(e.type_name_get()->type_get());
        }
    }
    // A RETIRER !!! type_default(e, &Void::instance());
    if (e.type_name_get())
      e.type_set(e.type_name_get()->type_get());
    else
      e.type_set(e.init_get()->type_get());
    
    // check si nameTy == nullptr
  // chopper le type de init_
  // comparer les types si nullptr != namety
  // si init_ == nil namety != nullptr
  // donner le type de vardec
  // FIXME: Some code was deleted here.
  }


  /*--------------------.
  | Visiting TypeDecs.  |
  `--------------------*/

  void
  TypeChecker::operator()(ast::TypeDecs& e)
  {
    decs_visit<ast::TypeDec>(e);
  }


  void
  TypeChecker::operator()(ast::TypeDec&)
  {
    // We must not be here.
    unreachable();
  }


  // Store this type.
  template <>
  void
  TypeChecker::visit_dec_header<ast::TypeDec>(ast::TypeDec& e)
  {
    // We only process the head of the type declaration, to set its
    // name in E.  A declaration has no type in itself; here we store
    // the type declared by E.

    //creer le type dec name_
    //l'assigner au typedec
    e.created_type_set(new Named(e.name_get()));
  // FIXME: Some code was deleted here.
  }

  // Bind the type body to its name.
  template <>
  void
  TypeChecker::visit_dec_body<ast::TypeDec>(ast::TypeDec& e)
  {
    type(e.ty_get());
    auto nam = dynamic_cast<const Named*>(e.created_type_get());
    if (nam)
      if (e.ty_get().type_get())
        nam->type_set(e.ty_get().type_get());
      else
        nam->type_set(e.ty_get().created_type_get());
    // FIXME: Some code was deleted here.
  }

  /*------------------.
  | Visiting /Decs/.  |
  `------------------*/

  template <class D>
  void
  TypeChecker::decs_visit(ast::AnyDecs<D>& e)
  {
    for (auto i : e.decs_get())
    {
      visit_dec_header<D>(*i);
    }
    for (auto i : e.decs_get())
    {
      visit_dec_body<D>(*i);
    }
  // FIXME: Some code was deleted here.
  }


  /*-------------.
  | Visit /Ty/.  |
  `-------------*/

  void
  TypeChecker::operator()(ast::NameTy& e)
  {
    if (e.name_get() == "string")
    {
      e.type_set(&String::instance());
    }
    else if (e.name_get() == "int")
    {
      e.type_set(&Int::instance());
    }
    else if (e.name_get() == "void")
    {
      e.type_set(&Void::instance());
    }
    else
    {
      if (e.def_get())
      {
        e.type_set(e.def_get()->created_type_get());
      }
    }
  // FIXME: Some code was deleted here (Recognize user defined types, and built-in types).
  }

  void
  TypeChecker::operator()(ast::RecordTy& e)
  {
    auto i = type(e.get_tyfields());
    e.created_type_set(i);
    // auto rec = new Record();
    // for (auto i : e.get_tyfields())
    //   {
    //  i->accept(*this);
    //  rec->field_add(i->name_get(), *type(i->type_name_get()));
    //   }
    // created_type_default(e, rec);
  // FIXME: Some code was deleted here.
  }

  void
  TypeChecker::operator()(ast::ArrayTy& e)
  {
    type(e.base_type_get());
    // created_type_default(e, e.base_type_get().type_get());
    created_type_default(e, new Array(e.base_type_get().name_get(),
                                      *e.base_type_get().type_get()));
  // FIXME: Some code was deleted here.
  }

} // namespace type

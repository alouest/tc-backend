/**
 ** \file type/array.cc
 ** \brief Implementation for type/array.hh.
 */

#include <ostream>

#include <type/array.hh>
#include <type/visitor.hh>

namespace type
{
  Array::Array(const misc::symbol& name, const Type& elt)
    : name_(name)
    , elt_(elt)
  {}

  void Array::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  void Array::accept(Visitor& v)
  {
    v(*this);
  }
  
  
  // misc::symbol& Array::name_get()
  // {
  //   return name_;
  // }
  const misc::symbol& Array::name_get() const
  {
    return name_;
  }
  // Type& Array::elt_get()
  // {
  //   return elt_;
  // }
  const Type& Array::elt_get() const
  {
    return elt_;
  }
  
  // FIXME: Some code was deleted here.

} // namespace type

/**
 ** \file type/record.cc
 ** \brief Implementation for type/record.hh.
 */

#include <ostream>

#include <type/builtin-types.hh>
#include <type/nil.hh>
#include <type/record.hh>
#include <type/visitor.hh>
#include <type/named.hh>

namespace type
{

  void
  Record::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  Record::accept(Visitor& v)
  {
    v(*this);
  }

  // FIXME: Some code was deleted here (Field manipulators).
  const Type* Record::field_type(const misc::symbol key) const
  {
    for (auto i : fields_)
      {
	if (key == i.name_get())
	  return &(i.type_get());
      }
  }

  int Record::field_index(const misc::symbol key) const
  {
    int j = 0;
    for (auto i : fields_)
      {
	if (key == i.name_get())
	  break;
	j++;
      }
    return j;
  }

  bool Record::compatible_with(const Type& other) const
  {
    auto nam = dynamic_cast<const Named*>(&other);
    if (nam)
      return &nam->actual() == this;
    auto nil = dynamic_cast<const Nil*>(&other);
    if (nil)
      return this->compatible_with(*nil->record_type_get());
    
    if (&other == this)
      return true;
    else
    {
      const Nil* child = dynamic_cast<const Nil*>(&other);
      return child != nullptr;
    }
  }

  bool Record::sound() const
  {
    auto i = fields_.begin();
    for (i; i != fields_.end(); i++)
    {
      const Named* child = dynamic_cast<const Named*>(&(i->type_get()));
      if (child != nullptr && child->actual() != *this)
      {
        const Record* child2 = dynamic_cast<const Record*>(&child->actual());
        if (!child2->sound())
          break;
      }
      else
      {
        break;
      }
    }
    return i == fields_.end();
  }
  // FIXME: Some code was deleted here (Special implementation of "compatible_with" for Record).

} // namespace type

/**
 ** \file type/named.cc
 ** \brief Implementation for type/named.hh.
 */

#include <ostream>
#include <set>

#include <type/named.hh>
#include <type/visitor.hh>

namespace type
{

  Named::Named(misc::symbol name)
    : name_(name)
    , type_(nullptr)
  {}

  Named::Named(misc::symbol name, const Type* type)
    : name_(name)
  {
    type_set(type);
  }

  bool Named::compatible_with(const Type& other) const
  {
    return (other.compatible_with(this->actual()));
  }

  bool Named::sound() const
  {
    const Named* child = dynamic_cast<const Named*>(type_);
    while (child != nullptr && child != this)
    {
      child = dynamic_cast<const Named*>(child->type_);
    }
    return (child == nullptr);
  }

  void Named::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void Named::accept(Visitor& v)
  {
    v(*this);
  }
  // FIXME: Some code was deleted here (Inherited functions).

  // FIXME: Some code was deleted here (Sound).

  // FIXME: Some code was deleted here (Special implementation of "compatible_with" for Named).

} // namespace type

/** \file translate/translation.cc
 ** \brief Implementation for translate/translation.hh.
 **/

#include <ast/op-exp.hh>
#include <temp/temp.hh>
#include <temp/label.hh>
#include <translate/access.hh>
#include <translate/exp.hh>
#include <translate/level.hh>
#include <translate/translation.hh>
#include <tree/fragment.hh>

namespace translate
{

  /*--------.
  | /Var/.  |
  `--------*/

  rExp
  simple_var(const Access& access, const Level& use_level)
  {
    return new Ex(access.exp(use_level));
  }

  rExp
  field_var(rExp var_exp, int index)
  {
    tree::rExp address =
      index == 0
      ? var_exp->un_ex()
      : new tree::Binop(tree::Binop::Oper::add,
                        var_exp->un_ex(),
                        new tree::Const(frame::word_size * index));
    return new Ex(new tree::Mem(address));
  }

  rExp
  subscript_var(rExp var_exp, rExp index_exp)
  {

    tree::rExp wd_size = int_exp(frame::word_size)->un_ex();
    tree::rExp ex = index_exp->un_ex();
    tree::Binop* mem = new tree::Binop(tree::Binop::Oper::mul,
				       ex, wd_size);

    return new Ex(new tree::Mem(new tree::Binop(
				tree::Binop::Oper::add,
				var_exp->un_ex(), mem)));
  // FIXME: Some code was deleted here.
  }


  /*--------.
  | /Exp/.  |
  `--------*/

  rExp
  nil_exp()
  {
    return new Ex(new tree::Const(0));
  }

  rExp
  int_exp(int i)
  {
    return new Ex(new tree::Const(i));
  // FIXME: Some code was deleted here.
  }

  rExp
  string_exp(const std::string& s, tree::Fragment*& f)
  {
    // const temp::Label& label = temp::Label(s);
    temp::Label l;
    f = new tree::DataFrag(l, s);
    return new Ex(new tree::Name(l));
  // FIXME: Some code was deleted here.
  }

  rExp
  record_exp(std::vector<rExp> fields)
  {
    // The record being built.
    tree::rTemp rec = new tree::Temp(temp::Temp());

    // Store the initialization.
    tree::rSeq init = new tree::Seq;

    // First allocate the memory.
    int record_size = fields.size() * frame::word_size;
    rExp malloc_call = call_exp("malloc", int_exp(record_size));
    init->emplace_back(new tree::Move(rec, malloc_call->un_ex()));
    tree::rSeq save = init;
    int j = 0;
    for (auto i : fields)
    {
      tree::rSeq temp = new tree::Seq
      {
        new tree::Move(new tree::Mem(new tree::Binop(tree::Binop::Oper::add, rec, new tree::Const(j * frame::word_size))), i->un_ex())
      };
      save->emplace_back(temp);
      save = temp;
      j++;
    }
  // FIXME: Some code was deleted here (Issue a move for each field of the record).

    return new Ex(new tree::Eseq(init, rec));
  }

  rExp
  static_link(const Level* decl, const Level* use)
  {
    assertion(decl);
    assertion(use);
    return new Ex(decl->parent_get()->fp(*use));
  }

  rExp
  call_exp(const temp::Label& label, std::vector<rExp> args)
  {
    std::vector<tree::rExp> args2;
    for (auto i : args)
    {
      args2.emplace_back(i->un_ex());
    }
    return new Ex(new tree::Call(new tree::Name(label), args2));
  // FIXME: Some code was deleted here.
  }

  rExp
  call_exp(const std::string& label, rExp arg1, rExp arg2)
  {
    std::vector<rExp> args = { arg1 };
    if (arg2)
      args.emplace_back(arg2);
    return call_exp(label, args);
  }


  // Operations on words, i.e., arithmetics, ordered comparison of
  // Integer, and object equality for Integers, Arrays and Records.
  rExp
  op_exp(const ast::OpExp::Oper oper, rExp left, rExp right)
  {
    if (oper == ast::OpExp::Oper::add)
      return new Ex(new tree::Binop(tree::Binop::Oper::add, left->un_ex(), right->un_ex()));
    else if (oper == ast::OpExp::Oper::sub)
      return new Ex(new tree::Binop(tree::Binop::Oper::sub, left->un_ex(), right->un_ex()));
    else if (oper == ast::OpExp::Oper::mul)
      return new Ex(new tree::Binop(tree::Binop::Oper::mul, left->un_ex(), right->un_ex()));
    else if (oper == ast::OpExp::Oper::div)
      return new Ex(new tree::Binop(tree::Binop::Oper::div, left->un_ex(), right->un_ex()));
    else if (oper == ast::OpExp::Oper::eq)
      return new Cx(tree::Cjump::Relop::eq, left->un_ex(), right->un_ex());
    else if (oper == ast::OpExp::Oper::ne)
      return new Cx(tree::Cjump::Relop::ne, left->un_ex(), right->un_ex());
    else if (oper == ast::OpExp::Oper::lt)
      return new Cx(tree::Cjump::Relop::lt, left->un_ex(), right->un_ex());
    else if (oper == ast::OpExp::Oper::gt)
      return new Cx(tree::Cjump::Relop::gt, left->un_ex(), right->un_ex());
    else if (oper == ast::OpExp::Oper::le)
      return new Cx(tree::Cjump::Relop::le, left->un_ex(), right->un_ex());
    else
      return new Cx(tree::Cjump::Relop::ge, left->un_ex(), right->un_ex());
  // FIXME: Some code was deleted here.
  }



  // Avoid useless structures.
  rExp
  seq_exp(std::vector<rExp>& exps)
  {
    switch (exps.size())
      {
      case 0:
        return new Nx(new tree::Sxp(new tree::Const(0)));
      case 1:
        return exps.back();
      default:
        tree::rSeq seq = new tree::Seq;
        for (const rExp& exp : exps)
          seq->emplace_back(exp->un_nx());
        return new Nx(seq);
      }
  }


  // Try to produce simple code for simple cases.
  rExp
  eseq_exp(std::vector<rExp>& exps)
  {
    if (exps.size() != 0)
      {
	auto last = exps.back();
	exps.pop_back();
	return new Ex(new tree::Eseq(seq_exp(exps)->un_nx(),
				     last->un_ex()));
      }
    else
      {
	return new Nx(new tree::Sxp(new tree::Const(0)));
      std::cout << "Premiere nouvelle" << std::endl;
      }
  // FIXME: Some code was deleted here.
  }

  rExp
  assign_exp(rExp dst, rExp src)
  {
    return new Nx(new tree::Move(dst->un_ex(), src->un_ex()));
  // FIXME: Some code was deleted here.
  }

  rExp
  if_exp(rExp test, rExp then_clause, rExp else_clause)
  {
    return new Ix(test, then_clause, else_clause);
  }



  rExp
  while_exp(rExp test, rExp body, const temp::Label& ldone)
  {
    temp::Label b;
    temp::Label tst;
    tree::rSeq init = new tree::Seq
    {
      new tree::Label(tst),
      test->un_cx(b, ldone),
      new tree::Label(b),
      body->un_nx(),
      new tree::Jump(new tree::Name(tst)),
      new tree::Label(ldone)
    };
    return new Nx(init);
  // FIXME: Some code was deleted here.
  }

  // FIXME: Some code was deleted here (for_exp -- "for" loop translation).

  rExp
  break_exp(const temp::Label& loop_end)
  {
    return new Nx(new tree::Jump(new tree::Name(loop_end)));
  }

  rExp
  array_exp(rExp array_size, rExp init_val)
  {
    // Call the runtime function which mallocs and initializes the
    // array.  This function returns a pointer to its result.
    rExp content = call_exp("init_array", array_size, init_val);

    temp::Temp array;
    return new Ex(new tree::Eseq(new tree::Move(new tree::Temp(array),
                                 content->un_ex()),
                                 new tree::Temp(array)));
  }


  /*--------.
  | /Dec/.  |
  `--------*/

  tree::ProcFrag*
  procedure_dec(const temp::Label& label, const misc::symbol& name,
                rExp body, frame::Frame* frame)
  {
    return new tree::ProcFrag(label, name, body->un_nx(), frame);
  }

  tree::ProcFrag*
  function_dec(const temp::Label& label, const misc::symbol& name,
               rExp value, frame::Frame* frame)
  {
    temp::Temp rv("rv");
    rExp body = new Nx(new tree::Move(new tree::Temp(rv),
                                      value->un_ex()));
    return procedure_dec(label, name, body, frame);
  }

} // namespace translate

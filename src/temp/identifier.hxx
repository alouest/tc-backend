/**                                                             -*- C++ -*-
 ** \file temp/identifier.hxx
 ** \brief Implementation of Identifier & TaggedIdentifier templates.
 */

#pragma once

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <variant>

#include <misc/contract.hh>
#include <temp/identifier.hh>

namespace temp
{

  /*-------------.
  | Identifier.  |
  `-------------*/

  template <template <typename Tag_> class Traits_>
  const misc::xalloc<typename Identifier<Traits_>::map_type*>
  Identifier<Traits_>::map;

  template <template <typename Tag_> class Traits_>
  std::ostream&
  operator<<(std::ostream& ostr, const Identifier<Traits_>& obj)
  {
    typename Identifier<Traits_>::map_type* m =
      Identifier<Traits_>::map(ostr);
    if (m)
      return (*m)(obj).dump(ostr);
    else
      return obj.dump(ostr);
  }


  template <template <typename Tag_> class Traits_>
  Identifier<Traits_>::Identifier()
    : value_(count_)
    , prefix_(&Traits_<unsigned>::prefix)
    , rank_(Traits_<unsigned>::rank)
  {
    ++count_;
  }

  template <template <typename Tag_> class Traits_>
  Identifier<Traits_>::Identifier(const misc::symbol& sym)
    : value_(sym)
    , prefix_(&Traits_<misc::symbol>::prefix)
    , rank_(Traits_<misc::symbol>::rank)
  {}


  template <template <typename Tag_> class Traits_>
  Identifier<Traits_>::Identifier(const std::string& str)
    : value_(str)
    , prefix_(&Traits_<misc::symbol>::prefix)
    , rank_(Traits_<misc::symbol>::rank)
  // FIXedME: Some code was deleted here.
  {}

  template <template <typename Tag_> class Traits_>
  Identifier<Traits_>::Identifier(const char* str)
    : value_(str)
    , prefix_(&Traits_<misc::symbol>::prefix)
    , rank_(Traits_<misc::symbol>::rank)
  // FIXedME: Some code was deleted here.
  {}

  template <template <typename Tag_> class Traits_>
  Identifier<Traits_>::Identifier(const Identifier<Traits_>& id)
    : value_(id.value_get())
    , prefix_(&id.prefix_get())
    , rank_(id.rank_get())
  // FIXedME: Some code was deleted here.
  {}


  template <template <typename Tag_> class Traits_>
  std::ostream&
  Identifier<Traits_>::dump(std::ostream& ostr) const
  {
    ostr << *prefix_ << value_;
    return ostr;
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  std::string
  Identifier<Traits_>::string_get() const
  {

    std::ostringstream str;
    str << *this;
    return str.str();
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  const typename Identifier<Traits_>::value_type&
  Identifier<Traits_>::value_get() const
  {
    return value_;
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  const std::string&
  Identifier<Traits_>::prefix_get() const
  {
    return *prefix_;
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  int
  Identifier<Traits_>::rank_get() const
  {
    return rank_;
  // FIXedME: Some code was deleted here.
  }


  template <template <typename Tag_> class Traits_>
  Identifier<Traits_>&
  Identifier<Traits_>::operator=(const Identifier<Traits_>& rhs)
  {
    rank_ = rhs.rank_get();
    prefix_ = &rhs.prefix_get();
    value_ = rhs.value_get();
    return *this;
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  bool
  Identifier<Traits_>::operator==(const Identifier<Traits_>& rhs) const
  {
    return
      rank_get() == rhs.rank_get()
      && std::visit(IdentifierEqualToVisitor(),
		    static_cast<std::variant<unsigned, misc::symbol>>(value_),
		    static_cast<std::variant<unsigned,
		    misc::symbol>>(rhs.value_));
    // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  bool
  Identifier<Traits_>::operator!=(const Identifier<Traits_>& rhs) const
  {
    return !(*this == rhs);
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  bool
  Identifier<Traits_>::operator<(const Identifier<Traits_>& rhs) const
  {
    return
      rank_get() < rhs.rank_get()
      || (rhs.rank_get() == rank_
	  && std::visit(IdentifierLessThanVisitor(),
			static_cast<std::variant<unsigned, misc::symbol>>(value_),
			static_cast<std::variant<unsigned,
			misc::symbol>>(rhs.value_)));
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  bool
  Identifier<Traits_>::operator<=(const Identifier<Traits_>& rhs) const
  {
    return (*this == rhs) || (*this < rhs);
  // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  bool
  Identifier<Traits_>::operator>(const Identifier<Traits_>& rhs) const
  {
    return !(*this < rhs) && !(*this == rhs);
    // FIXedME: Some code was deleted here.
  }

  template <template <typename Tag_> class Traits_>
  bool
  Identifier<Traits_>::operator>=(const Identifier<Traits_>& rhs) const
  {
    return !(*this < rhs);
  // FIXedME: Some code was deleted here.
  }


  /*------------------.
  | Variant visitor.  |
  `------------------*/

  // FIXedME: Some code was deleted here.
  template <template <typename Elt_> class Cmp_>
  template <typename T, typename U>
  bool
  IdentifierCompareVisitor<Cmp_>
  ::operator()(const T&, const U&) const
  {
    abort();
    return false;
  }

  template <template <typename Elt_> class Cmp_>
  template <typename T>
  bool
  IdentifierCompareVisitor<Cmp_>
  ::operator()(const T& lhs, const T& rhs) const
  {
    Cmp_ c;
    return c(lhs, rhs);
  }

} // namespace temp

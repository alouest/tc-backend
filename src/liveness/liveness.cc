/**
 ** \file liveness/liveness.cc
 ** \brief Implementation of liveness::Liveness.
 */

#include <ostream>
#include <set>
#include <vector>

/* Help Boost 1.50 find boost::disable_if, required by
   boost/graph/reverse_graph.hpp (fixed in Boost 1.51).  */
#include <boost/graph/reverse_graph.hpp>
#include <boost/utility/enable_if.hpp>

#include <liveness/liveness.hh>
#include <temp/temp.hh>

namespace liveness
{

  Liveness::Liveness(const std::string& name,
                     const assem::Instrs& instrs,
                     const temp::TempMap& tempmap)
    : FlowGraph<temp::temp_set_type>(name, instrs)
  {
    auto w = this->topological_sort();
    while (!w.empty())
    {
      vertex_descriptor n = w.front();
      w.erase(w.begin());
      temp::temp_set_type old = in.find(n)->second;
      // begin out[n] <- gen[n] U (int -kill[n]) 
      for (auto [i, end] = boost::adjacent_vertices(n, *this); i != end; ++i)
      {
	out[n] += in[*i];
      }
      // end
      if (in.find(n) != in.end())
      {
        for (auto i : (*this)[n]->use())
        {
          in.find(n)->second.insert(tempmap[i]);
        }
      }
      else
      {
        in.emplace(std::make_pair(n, temp::temp_set_type()));
        for (auto i : (*this)[n]->use())
        {
          in.find(n)->second.insert(tempmap[i]);
        }
      }
      temp::temp_set_type tmp;
      if (out.find(n) != out.end())
      {
        for (auto i : out.find(n)->second)
          tmp.insert(tempmap[i]);
      }
      for (auto i : (*this)[n]->def())
      {
        tmp.erase(tempmap[i]);
      }
      in.find(n)->second.merge(tmp);
      if (old != in.find(n)->second)
      {
        for (auto j = in_edges(n, *this); j.first != j.second; ++j.first)
        {
          if (std::find(w.begin(), w.end(), source(*j.first, *this)) == w.end())
          {
            w.push_back(source(*j.first, *this));
          }
        }
      }

    }
  // FIXME: Some code was deleted here (Compute IN and OUT).

    // Label the edges with the list of live temps, to aid debugging.
    timer_.push("8: liveness edges");
    for (auto [e, end] = boost::edges(*this); e != end; ++e)
      (*this)[*e] = misc::set_intersection(out[boost::source(*e, *this)],
                                           in[boost::target(*e, *this)]);
    timer_.pop("8: liveness edges");
  }

  const Liveness::livemap_type&
  Liveness::liveout_get() const
  {
    return out;
  }

  const Liveness::livemap_type&
  Liveness::livein_get() const
  {
    return in;
  }

} // namespace regalloc

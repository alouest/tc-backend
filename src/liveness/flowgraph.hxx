/**
 ** \file liveness/flowgraph.hxx
 ** \brief Implementation for liveness/flowgraph.hh.
 */

#pragma once

#include <map>

#include <common.hh>
#include <misc/contract.hh>
#include <liveness/flowgraph.hh>

namespace liveness
{

  template <typename EdgeLabel>
  FlowGraph<EdgeLabel>::FlowGraph(const std::string& name,
                                  const assem::Instrs& instrs)
  {
    precondition(!instrs.empty());

    timer_.push("8: flow graph");
    this->name_set(name);

    // Create one node for each instruction and collect the labels in a map.
    // (The first node of the graph must be the first instruction).
  // FIXedME: Some code was deleted here.
    std::map<temp::Label, vertex_descriptor> all;
    for (auto it = instrs.begin(); it != instrs.end(); ++it)
    {
      auto ver = this->vertex_add(*it);
      auto lab = dynamic_cast<assem::Label*>(*it);
      if (lab != nullptr)
        all.insert_or_assign(lab->label_get(), ver);
    }
    // Create an edge between each branching instruction and
    // its destination labels.
  // FIXedME: Some code was deleted here.
    for (auto it = vertices(*this); it.first != it.second; ++it.first)
    {
      auto tmp = ++it.first;
      it.first--;
      if (((*this)[*it.first])->jumps().size() == 0 ||
          ((*this)[*it.first])->use().size() != 0)
      {
        if (tmp != it.second)
          this->edge_add(*it.first, *tmp);
      }
      for (auto jmp : ((*this)[*it.first])->jumps())
      {
        auto res = all.find(jmp);
        if (res != all.end())
          this->edge_add(*it.first, res->second);
      }
    }
    timer_.pop("8: flow graph");
  }

  template <typename EdgeLabel>
  const misc::timer&
  FlowGraph<EdgeLabel>::timer_get() const
  {
    return timer_;
  }

  template <typename EdgeLabel>
  std::ostream&
  FlowGraph<EdgeLabel>::vertex_print(vertex_descriptor v,
                                     std::ostream& ostr) const
  {
    return ostr << *(*this)[v];
  }

} // namespace liveness

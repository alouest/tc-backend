                                                            /* -*- C++ -*- */
%option c++
%option nounput
%option debug
%option batch

%{

#include <cerrno>
#include <climits>
#include <regex>
#include <string>

#include <boost/lexical_cast.hpp>

#include <misc/contract.hh>
  // Using misc::escape is very useful to quote non printable characters.
  // For instance
  //
  //    std::cerr << misc::escape('\n') << '\n';
  //
  // reports about `\n' instead of an actual new-line character.
#include <misc/escape.hh>
#include <misc/symbol.hh>
#include <parse/parsetiger.hh>
#include <parse/tiger-parser.hh>

  // FIXME: Some code was deleted here.
  int depth = 0;
  // Debug mode handling


  int yy_flex_debug = (std::getenv("SCAN") != nullptr) && (*std::getenv("SCAN") == 1);

  
// Convenient shortcuts.
#define TOKEN_VAL(Type, Value)                  \
  parser::make_ ## Type(Value, tp.location_)

#define YY_USER_ACTION                  \
  do {                                  \
    tp.location_.columns(yyleng);       \
  } while (false);


#define TOKEN(Type)                             \
  parser::make_ ## Type(tp.location_)


// Flex uses `0' for end of file.  0 is not a token_type.
#define yyterminate() return TOKEN(EOF)

# define CHECK_OBJECT()                                 \
  do {                                                  \
    if (!tp.enable_object_extensions_p_)                \
      tp.error_ << misc::error::error_type::scan        \
                << tp.location_                         \
                << ": object extansion not activated.";	\
  } while(false)
  
# define CHECK_EXTENSION()                              \
  do {                                                  \
    if (!tp.enable_extensions_p_)                       \
      tp.error_ << misc::error::error_type::scan        \
                << tp.location_                         \
                << ": invalid identifier: `"            \
                << misc::escape(yytext) << "'\n";       \
  } while (false)

YY_FLEX_NAMESPACE_BEGIN
%}

%x SC_COMMENT SC_STRING

/* Abbreviations.  */
BLANK    [ \t]
INT      [0-9]+
ID       [a-zA-Z][a-zA-Z0-9_]*|"_main"|"_hi"|"_lo"
LINE     "\r\n"|"\n"|"\r"|"\n\r"
ESCAPE   "\\a"|"\\b"|"\\\""|"\\f"|"\\n"|"\\r"|"\\t"|"\\v"|"\\num"|"\\xnum"

%%
%{
  std::string str = "";
  // FIXME: Some code was deleted here (Local variables).
  // Each time yylex is called.
  tp.location_.step();
%}/* STATE DEFINITION */
<SC_COMMENT>
{
  {LINE}    { }
  "/*"      { depth ++; }
  "*/"      {
              depth --;
              if (depth  <= 0)
	        BEGIN (INITIAL); }
  <<EOF>>   {
              std::cerr << tp.location_ << ": unexpected end of file in a comment"
			<< std::endl;
	      exit(2);
            }
  .         { }
}


<SC_STRING>
{
  {LINE}    { }
  "\\n"  { str = str.append("\n"); }
  "\\a"  { str = str.append("\a"); }
  "\\b"  { str = str.append("\b"); }
  "\\\""  { str = str.append("\""); }
  "\\f"  { str = str.append("\f"); }
  "\\r"  { str = str.append("\r"); }
  "\\t"  { str = str.append("\t"); }
  "\\v"  { str = str.append("\v"); }
  "\\\\"  { str = str.append("\v"); }
  \0[0-3][0-7]{2}  { str = str.append(1, std::strtol(yytext + 1, 0, 8)); }
  "\\x"[0-9A-Fa-f]{2} {str.append(1, std::strtol(yytext + 2, 0, 16)); }
  "\""      { BEGIN (INITIAL); return TOKEN_VAL(STRING, str);}
  "\\"      {
              
              std::cerr  << tp.location_ << ": Unrecognized escape : "
			 << yytext << std::endl;
	      exit(2);
            }
  <<EOF>>   {
              std::cerr  << tp.location_ << ": unfinished string" << std::endl;
	      exit(2);
            }
  .         { str = str.append(yytext); }
}
 /* The rules.  */

{BLANK}     { }
{LINE}      { }
{INT}       {
              try
		{
		  auto rand = 0;
		  rand = std::stoi(yytext);
		  return TOKEN_VAL(INT, rand);
		}
	      catch (std::out_of_range& e)
		{
		  std::cerr  << tp.location_ << ": Integer out of bound : "
			     << yytext << std::endl;
		  exit(2);
		}
            }

"array"     { return TOKEN(ARRAY); }
"if"        { return TOKEN(IF); }
"then"      { return TOKEN(THEN); }
"else"      { return TOKEN(ELSE); }
"while"     { return TOKEN(WHILE); }
"for"       { return TOKEN(FOR); }
"to"        { return TOKEN(TO); }
"do"        { return TOKEN(DO); }
"let"       { return TOKEN(LET); }
"in"        { return TOKEN(IN); }
"end"       { return TOKEN(END);}
"of"        { return TOKEN(OF); }
"break"     { return TOKEN(BREAK); }
"nil"       { return TOKEN(NIL); }
"function"  { return TOKEN(FUNCTION); }
"var"       { return TOKEN(VAR); }
"type"      { return TOKEN(TYPE); }
"import"    { return TOKEN(IMPORT); }
"primitive" { return TOKEN(PRIMITIVE); }
"class"     { CHECK_OBJECT();
              return TOKEN(CLASS); }
"extends"   { CHECK_OBJECT();
              return TOKEN(EXTENDS); }
"method"    { CHECK_OBJECT();
              return TOKEN(METHOD); }
"new"       { CHECK_OBJECT();
              return TOKEN(NEW); }
"/*"        {
              depth ++;
 	      BEGIN(SC_COMMENT);
    	    }
","         { return TOKEN(COMMA); }
":"         { return TOKEN(COLON); }
";"         { return TOKEN(SEMI); }
"("         { return TOKEN(LPAREN); }
")"         { return TOKEN(RPAREN); }
"["         { return TOKEN(LBRACK); }
"]"         { return TOKEN(RBRACK); }
"{"         { return TOKEN(LBRACE); }
"}"         { return TOKEN(RBRACE); }
"."         { return TOKEN(DOT); }
"+"         { return TOKEN(PLUS); }
"-"         { return TOKEN(MINUS); }
"*"         { return TOKEN(TIMES); }
"/"         { return TOKEN(DIVIDE); }
"="         { return TOKEN(EQ); }
":="        { return TOKEN(ASSIGN); }
"<>"        { return TOKEN(NE); }
"<"         { return TOKEN(LT); }
">"         { return TOKEN(GT); }
"<="        { return TOKEN(LE); }
">="        { return TOKEN(GE); }
"&"         { return TOKEN(AND); }
"|"         { return TOKEN(OR); }
"\""        { BEGIN (SC_STRING); }
{ID}        { return TOKEN_VAL(ID, yytext); }
"_decs"     { return TOKEN(DECS); }
"_cast"     { return TOKEN(CAST); }
"_exp"      { return TOKEN(EXP); }
"_lvalue"   { return TOKEN(LVALUE); }
"_namety"   { return TOKEN(NAMETY); }
.           {
              std::cerr  << tp.location_ << ": scan error, unexpected "
			 << yytext << std::endl;
	      exit(2);
	    }
  /* FIXME: Some code was deleted here. */
%%

// Do not use %option noyywrap, because then flex generates the same
// definition of yywrap, but outside the namespaces, so it defines it
// for ::yyFlexLexer instead of ::parse::yyFlexLexer.
int yyFlexLexer::yywrap() { return 1; }

void
yyFlexLexer::scan_open_(std::istream& f)
{
  yypush_buffer_state(YY_CURRENT_BUFFER);
  yy_switch_to_buffer(yy_create_buffer(&f, YY_BUF_SIZE));
}

void
yyFlexLexer::scan_close_()
{
  yypop_buffer_state();
}

YY_FLEX_NAMESPACE_END

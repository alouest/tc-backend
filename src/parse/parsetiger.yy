                                                                // -*- C++ -*-
%require "3.0"
%language "C++"
// Set the namespace name to `parse', instead of `yy'.
%name-prefix "parse"
%define api.value.type variant
%define api.token.constructor

  // FIXME: Some code was deleted here (Other directives: %skeleton "lalr1.cc" %expect 0 etc).
%error-verbose
%defines
%debug
%skeleton "glr.cc"
%glr-parser
%expect-rr 0
%expect 2
 // Prefix all the tokens with TOK_ to avoid colisions.
%define api.token.prefix {TOK_}
/* We use pointers to store the filename in the locations.  This saves
   space (pointers), time (no deep copy), but leaves the problem of
   deallocation.  This would be a perfect job for a misc::symbol
   object (passed by reference), however Bison locations require the
   filename to be passed as a pointer, thus forcing us to handle the
   allocation and deallocation of this object.

   Nevertheless, all is not lost: we can still use a misc::symbol
   object to allocate a flyweight (constant) string in the pool of
   symbols, extract it from the misc::symbol object, and use it to
   initialize the location.  The allocated data will be freed at the
   end of the program (see the documentation of misc::symbol and
   misc::unique).  */
%define filename_type {const std::string}
%locations

// The parsing context.
%param { ::parse::TigerParser& tp }

/*---------------------.
| Support for tokens.  |
`---------------------*/
%code requires
{
#include <string>
#include <misc/algorithm.hh>
#include <misc/separator.hh>
#include <misc/symbol.hh>
#include <parse/fwd.hh>
  // Pre-declare parse::parse to allow a ``reentrant'' parsing within
  // the parser.
  namespace parse
  {
    ast_type parse(Tweast& input);
  }
}

%code provides
{
  
  // Announce to Flex the prototype we want for lexing (member) function.
  # define YY_DECL_(Prefix)                               \
    ::parse::parser::symbol_type                          \
    (Prefix parselex)(::parse::TigerParser& tp)
  # define YY_DECL YY_DECL_(yyFlexLexer::)
}

%printer { yyo << $$; } <int> <std::string> <misc::symbol>;

%token <std::string>    STRING "string"
%token <misc::symbol>   ID     "identifier"
%token <int>            INT    "integer"


/*--------------------------------.
| Support for the non-terminals.  |
`--------------------------------*/

%code requires
{
# include <ast/fwd.hh>
// Provide the declarations of the following classes for the
// %destructor clauses below to work properly.
# include <ast/exp.hh>
# include <ast/var.hh>
# include <ast/ty.hh>
# include <ast/name-ty.hh>
# include <ast/field.hh>
# include <ast/field-init.hh>
# include <ast/function-dec.hh>
# include <ast/type-dec.hh>
# include <ast/var-dec.hh>
# include <ast/any-decs.hh>
# include <ast/decs-list.hh>
}

  // FIXME: Some code was deleted here (Printers and destructors).


/*-----------------------------------------.
| Code output in the implementation file.  |
`-----------------------------------------*/

%code
{
# include <parse/tiger-parser.hh>
# include <parse/scantiger.hh>
# include <parse/tweast.hh>
# include <misc/separator.hh>
# include <misc/symbol.hh>
# include <ast/all.hh>
# include <ast/libast.hh>

  namespace
  {  
    /// Get the metavar from the specified map.
    template <typename T>
    T*
    metavar(parse::TigerParser& tp, unsigned key)
    {
      parse::Tweast* input = tp.input_;
      return input->template take<T>(key);
    }

  }

  /// Use our local scanner object.
  inline
  ::parse::parser::symbol_type
  parselex(parse::TigerParser& tp)
  {
    return tp.scanner_->parselex(tp);
  }
}

// Definition of the tokens, and their pretty-printing.
%token AND          "&"
       ARRAY        "array"
       ASSIGN       ":="
       BREAK        "break"
       CAST         "_cast"
       CLASS        "class"
       COLON        ":"
       COMMA        ","
       DIVIDE       "/"
       DO           "do"
       DOT          "."
       ELSE         "else"
       END          "end"
       EQ           "="
       EXTENDS      "extends"
       FOR          "for"
       FUNCTION     "function"
       GE           ">="
       GT           ">"
       IF           "if"
       IMPORT       "import"
       IN           "in"
       LBRACE       "{"
       LBRACK       "["
       LE           "<="
       LET          "let"
       LPAREN       "("
       LT           "<"
       MINUS        "-"
       METHOD       "method"
       NE           "<>"
       NEW          "new"
       NIL          "nil"
       OF           "of"
       OR           "|"
       PLUS         "+"
       PRIMITIVE    "primitive"
       RBRACE       "}"
       RBRACK       "]"
       RPAREN       ")"
       SEMI         ";"
       THEN         "then"
       TIMES        "*"
       TO           "to"
       TYPE         "type"
       VAR          "var"
       WHILE        "while"
       EOF 0        "end of file"
       CHUNK        "chunk"
       DECS         "_decs"
       EXP          "_exp"
       LVALUE       "_lvalue"
       NAMETY       "_namety"


%right ELSE THEN
%nonassoc TO DO OF ASSIGN
%left OR
%left AND
%nonassoc NE GT LT LE GE EQ 
%left "+" "-" 
%left "*" "/" "(" ")" "[" "]" "{" "}"
%left "chunk"
%left "function" "method" "class" "type" "var"
%left "primitive"

%type <ast::Exp*> exp
%type <ast::DecsList*> decs
%type <std::vector<ast::FieldInit*>*> recordfields
%type <std::vector<ast::FieldInit*>*> recordfields2
%type <std::vector<ast::Exp*>*> argfields
%type <ast::Var*> lvalue
%type <ast::Var*> lvalue2
%type <std::vector<ast::Exp*>*> exps
%type <std::vector<ast::Exp*>*> exps2
%type <ast::TypeDecs*> typedecs
%type <ast::TypeDec*> typedec
%type <ast::VarDecs*> vardecs
%type <ast::VarDec*> vardec
%type <ast::FunctionDecs*> functiondecs
%type <ast::FunctionDec*> functiondec
%type <ast::DecsList*> classfields
%type <ast::MethodDecs*> classmethods
%type <ast::MethodDec*> classmethod
%type <ast::VarDecs*> classvar
%type <ast::VarDecs*> funcfields
%type <ast::VarDecs*> funcfields2
%type <ast::Ty*> ty
%type <std::vector<ast::Field*>*> tyfields
%type <std::vector<ast::Field*>*> tyfields2
%type <ast::NameTy*> type-id
%destructor { delete $$; } <ast::Exp*> <ast::Dec*> <ast::Field*> <ast::FieldInit*> <ast::Ty*> <ast::DecsList*> <ast::Var*>
%destructor { for (auto i : *$$)
                delete i;
              delete $$;
            } <std::vector<ast::Field*>*> <std::vector<ast::FieldInit*>*>  <std::vector<ast::Exp*>*> 
  // FIXME: Some code was deleted here (More %types).

  // FIXME: Some code was deleted here (Priorities/associativities).
%start program

%%


program:
  /* Parsing a source program.  */
  exp   { tp.ast_ = $1; }
| /* Parsing an imported file.  */
  decs  { tp.ast_ = $1; }
;

recordfields :
         ID "=" exp recordfields2 {
           $4->insert($4->begin(), new ast::FieldInit(@$, $1, $3));
           $$ = $4;
         }
         | error recordfields2 { $$ = $2; }
         ;

recordfields2 :
          %empty { $$ = new std::vector<ast::FieldInit*>(); }
          | "," ID "=" exp recordfields2 { $5->insert($5->begin(), new ast::FieldInit(@$, $2, $4)); $$ = $5; }
          | "," error recordfields2 { $$ = $3; }
          ;

argfields :
          exp {
            auto vect = new std::vector<ast::Exp*>(); 
            vect->insert(vect->begin(), $1);
            $$ = vect;
          }
          | exp "," argfields { $3->insert($3->begin(), $1); $$ = $3; }
          | error "," argfields { $$ = $3; }
          ;

exp :
    "nil" { $$ = new ast::NilExp(@$); }
    | "_exp" "(" INT ")" { $$ = metavar<ast::Exp>(tp, $3); }
    | "_cast" "(" exp "," ty ")" { $$ = new ast::CastExp(@$, $3, $5); }
    | INT {
            auto ret = new ast::IntExp(@$, $1);
            $$ = ret;
          }
    | STRING {
               auto ret = new ast::StringExp(@$, $1);
	       $$ = ret;
             }
    | type-id "[" exp "]" "of" exp { $$ = new ast::ArrayExp(@$, $3, $6, $1); }
    | type-id "{" recordfields "}" { $$ = new ast::RecordExp(@$, $1, $3); }
    | type-id "{" "}" { $$ = new ast::RecordExp(@$, $1, new std::vector<ast::FieldInit*>()); }
    | "new" type-id { $$ = new ast::ObjectExp(@$, $2); }
    | lvalue { $$ = $1; }
    | ID "(" ")" {$$ = new ast::CallExp(@$, $1, nullptr);}
    | ID "(" argfields ")" { $$ = new ast::CallExp(@$, $1, $3); }
    | lvalue "." ID "(" ")" { $$ = new ast::MethodCallExp(@$, $3, nullptr, $1); }
    | lvalue "." ID "(" argfields ")" { $$ = new ast::MethodCallExp(@$, $3, $5, $1); }
    | "-" exp { $$ = new ast::OpExp(@$, new ast::IntExp(@$, 0), ast::OpExp::Oper::sub, $2); }
    | exp "+" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::add, $3); }
    | exp "-" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::sub, $3); }
    | exp "*" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::mul, $3); }
    | exp "/" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::div, $3); }
    | exp "=" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::eq, $3); }
    | exp "<>" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::ne, $3); }
    | exp "<" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::lt, $3); }
    | exp ">" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::gt, $3); }
    | exp "<=" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::le, $3); }
    | exp ">=" exp { $$ = new ast::OpExp(@$, $1, ast::OpExp::Oper::ge, $3); }
    | exp "&" exp  { $$ = new ast::IfExp(@$, $1, new ast::OpExp(@$, $3, ast::OpExp::Oper::ne, new ast::IntExp(@$, 0)), new ast::IntExp(@$, 0));}
    | exp "|" exp { $$ = new ast::IfExp(@$, $1, new ast::IntExp(@$, 1), $3); } 
    | "(" exps ")" { $$ = new ast::SeqExp(@$, $2); }
    | lvalue ":=" exp { $$ = new ast::AssignExp( @$, $1, $3); }
    | "if" exp "then" exp "else" exp { $$ = new ast::IfExp(@$, $2, $4, $6); }
| "if" exp "then" exp { $$ = new ast::IfExp(@$, $2, $4, nullptr); }
    | "while" exp "do" exp { $$ = new ast::WhileExp(@$, $2, $4); }
    | "for" ID ":=" exp "to" exp "do" exp { $$ = new ast::ForExp(@$,
                                                 new ast::VarDec(@$, $2, nullptr, $4), $6, $8); }
    | "break" { $$ = new ast::BreakExp(@$); }
| "let" decs "in" exps "end" { $$ = new ast::LetExp(@$, $2, new ast::SeqExp(@$, $4)); }
    ;

lvalue :
       ID { $$ = new ast::SimpleVar(@$, $1); }
       | lvalue2 { $$ = $1; }
       ;

lvalue2 :
        ID "[" exp "]"  { $$ = new ast::SubscriptVar(@$, new ast::SimpleVar(@$, $1), $3); }
        | "_lvalue" "(" INT ")" { $$ = metavar<ast::Var>(tp, $3);}
        | "_cast" "(" lvalue "," ty ")" { $$ = new ast::CastVar(@$, $3, $5); }
        | lvalue "." ID { $$ = new ast::FieldVar(@$, $1, $3); }
        | lvalue2 "[" exp "]" { $$ = new ast::SubscriptVar(@$, $1, $3); }
        ;

exps :
     %empty { $$ = new std::vector<ast::Exp*>(); }
     | exp exps2 { $2->insert($2->begin(), $1); $$ = $2; }
     ;

exps2 :
     %empty { $$ = new std::vector<ast::Exp*>(); }
     | ";" exp exps2 { $3->insert($3->begin(), $2); $$ = $3; }
     | ";" error exps2 { $$ = $3; }
     ;

decs :
     %empty { $$ = new ast::DecsList(@$); }
     | "_decs" "(" INT ")" decs { $5->splice_front(*metavar<ast::DecsList>(tp, $3));
	  $$ = $5; }
     | vardecs decs { $2->push_front($1); $$ = $2; }
     | functiondecs decs { $2->push_front($1); $$ = $2; }
     | typedecs decs { $2->push_front($1); $$ = $2; }
     | error decs {$$ = $2; }
     | "import" STRING decs {
       auto ret = tp.parse_import ($2, @$);
       if (ret == nullptr)
         ret = new ast::DecsList(@$);
       $3->splice_front(*ret);
       delete ret;
       $$ = $3;
       }
     ;

typedecs :
    typedec %prec "chunk" {
      auto vect = new ast::TypeDecs(@$);
      vect->push_front(*$1);
      $$ = vect;
    }
    | typedec typedecs { $2->push_front(*$1); $$ = $2; }
    ;

typedec :
    "type" ID "=" ty {$$ = new ast::TypeDec(@$, $2, $4); }
    | "class" ID "{" classfields "}" {
      auto obj = new ast::ClassTy(@$, new ast::NameTy(@$, "Object"), $4);
      $$ = new ast::TypeDec(@$, $2, obj);
    }
    | "class" ID "extends" ID "{" classfields "}" {
      auto obj = new ast::ClassTy(@$, new ast::NameTy(@$, $4), $6);
      $$ = new ast::TypeDec(@$, $2, obj);
    }
    ;

functiondecs :
    functiondec %prec "chunk" {
      auto vect = new ast::FunctionDecs(@$);
      vect->push_front(*$1);
      $$ = vect;
    }
    | functiondec functiondecs { $2->push_front(*$1); $$ = $2; }
    ;

functiondec :
    "function" ID "(" funcfields ")" "=" exp { $$ = new ast::FunctionDec(@$, $2, $4, nullptr, $7); }
    | "function" ID "(" funcfields ")" ":" type-id "=" exp  { $$ = new ast::FunctionDec(@$, $2, $4, $7, $9); }
    | "primitive" ID "(" funcfields ")" { $$ = new ast::FunctionDec(@$, $2, $4, nullptr, nullptr); }
    | "primitive" ID "(" funcfields ")" ":" type-id { $$ = new ast::FunctionDec(@$, $2, $4, $7, nullptr); }
    ;

vardecs :
    vardec {
      auto vect = new ast::VarDecs(@$);
      vect->push_front(*$1);
      $$ = vect;
    }
    ;
    
vardec :
    "var" ID ":" type-id ":=" exp { $$ = new ast::VarDec(@$, $2, $4, $6);}
    | "var" ID ":=" exp { $$ = new ast::VarDec(@$, $2, nullptr, $4);}
    ;

classfields :
            %empty {$$ = new ast::DecsList(@$);}
            | classvar classfields { $2->push_front($1); $$ = $2; }
            | classmethods classfields {$2->push_front($1); $$ = $2; }
            ;

classvar :
         vardec {
           auto dec = new ast::VarDecs(@$);
           dec->push_front(*$1);
           $$ = dec;
         }
         ;

classmethods :
             classmethod %prec "chunk" {
               auto vect = new ast::MethodDecs(@$);
               vect->push_front(*$1);
               $$ = vect;
             }
             | classmethod classmethods { $2->push_front(*$1); $$ = $2;} 
             ;

classmethod :
            "method" ID "(" funcfields ")" "=" exp { $$ = new ast::MethodDec(@$, $2, $4, nullptr, $7); }
            | "method" ID "(" funcfields ")" ":" type-id "=" exp { $$ = new ast::MethodDec(@$, $2, $4, $7, $9); }
            ;

funcfields :
           %empty { $$ = new ast::VarDecs(@$); }
           | ID ":" type-id funcfields2 {
             auto var = new ast::VarDec(@$, $1, $3, nullptr);
               $4->push_front(*var);
               $$ = $4;
             }
           ;

funcfields2 :
            %empty { $$ = new ast::VarDecs(@$); }
            | "," ID ":" type-id funcfields2  {
               auto var = new ast::VarDec(@$, $2, $4, nullptr);
               $5->push_front(*var);
               $$ = $5;
             }
            ;

ty :
   type-id { $$ = $1; }
   | "{" tyfields "}" { $$ = new ast::RecordTy(@$, $2); }
   | "array" "of" type-id { $$ = new ast::ArrayTy(@$, $3); }
   | "class" "{" classfields "}" { $$ = new ast::ClassTy(@$, new ast::NameTy(@$, "Object"), $3); }
   | "class" "extends" type-id "{" classfields "}" { $$ = new ast::ClassTy(@$, $3, $5); }
   ;

tyfields :
         %empty { $$ = new std::vector<ast::Field*>(); } 
         | ID ":" type-id tyfields2 {$4->insert($4->begin(), new ast::Field(@$, $1, $3)); $$= $4; }
         ;

tyfields2 :
          %empty { $$= new std::vector<ast::Field*>(); }
          | "," ID ":" type-id tyfields2 {$5->insert($5->begin(), new ast::Field(@$, $2, $4)); $$= $5; }
          ;

type-id :
        ID { $$ = new ast::NameTy(@$, $1); }
        |"_namety" "(" INT ")" { $$ = metavar<ast::NameTy>(tp, $3); }
        ;
%%



void
parse::parser::error(const location_type& l, const std::string& m)
{
  tp.error_ << misc::error::error_type::parse << l << ": Parsing error: " << m << std::endl;

}

dist_noinst_DATA +=				\
  %D%/runtime.s

CLEANFILES += %D%/runtime.cc
%D%/runtime.cc: %D%/runtime.s
	$(AM_V_GEN)$(AWK)						\
	   'BEGIN {							\
	     print("#include \"target/arm/arm-assembly.hh\"");		\
	     print("");							\
	     print("namespace target {");				\
	     print("namespace arm {");					\
	     print("const char* ArmAssembly::runtime(bool gc) const");	\
	     print("{");						\
	     print("  if (!gc)");					\
	     print("    return \"\\");					\
	   }								\
	   /^\#(<<|>>)/ {						\
	     next;							\
	   }								\
	   {								\
	     gsub(/[\\\"]/, "\\\\&", $$0);				\
	     print($$0 "\\n\\");					\
	   }								\
	 END { 								\
			 print("\";");							\
	 }'								\
	  $< >$@.tmp
	$(AM_V_at)$(AWK)						\
	   'BEGIN {							\
	     print("  else");						\
	     print("    return \"\\");					\
	   }								\
	   /^\#(<<|>>)/ {						\
	     next;							\
	   }								\
	   {								\
	     gsub(/[\\\"]/, "\\\\&", $$0);				\
	     gsub(/call[ \t]+malloc/, "call\tGC_malloc", $$0);		\
	     print($$0 "\\n\\");					\
	   }								\
	END { 								\
	  print("\";");							\
	  print("}");							\
	  print("} // namespace arm");					\
	  print("} // namespace target");				\
	  print("");							\
	}'								\
	  $< >>$@.tmp
	$(AM_V_at)mv $@.tmp $@

BUILT_SOURCES += %D%/arm-codegen.hh %D%/arm-codegen.cc

EXTRA_DIST +=					\
  $(ARM_CODEGEN_GRAMMAR)			\
  %D%/prologue.hh			\
  %D%/epilogue.cc
ARM_CODEGEN_GRAMMAR =				\
  %D%/binop.brg				\
  %D%/call.brg				\
  %D%/cjump.brg				\
  %D%/exp.brg				\
  %D%/mem.brg				\
  %D%/move.brg				\
  %D%/move_store.brg			\
  %D%/move_load.brg			\
  %D%/stm.brg				\
  %D%/temp.brg				\
  %D%/tree.brg

EXTRA_DIST += $(srcdir)/%D%/arm-codegen.stamp
# The dependency is on monoburg++.in and not monoburg++, since
# monoburg++ is regenerated at distribution time, and voids the time
# stamps (which we don't want!).
$(srcdir)/%D%/arm-codegen.stamp: $(ARM_CODEGEN_GRAMMAR) $(srcdir)/%D%/prologue.hh \
			$(srcdir)/%D%/epilogue.cc $(MONOBURGXX_IN)
	$(AM_V_GEN)$(MAKE) $(AM_MAKEFLAGS) $(MONOBURGXX)
	$(AM_V_at)rm -f $@ $@.tmp
	$(AM_V_at)touch $@.tmp
	$(AM_V_at)$(MONOBURGXX) $(srcdir)/%D%/tree.brg    \
		-r $(srcdir)/src \
	  -s $(srcdir)/%D%/arm-codegen.cc \
		-d $(srcdir)/%D%/arm-codegen.hh \
	  -n TARGET_ARM_CODEGEN --quiet
	$(AM_V_at)mv -f $@.tmp $@
$(srcdir)/%D%/arm-codegen.hh $(srcdir)/%D%/arm-codegen.cc: $(srcdir)/%D%/arm-codegen.stamp
	$(AM_V_GEN)if test -f $@; then				\
	  touch $@;						\
	else							\
	  rm -f $(srcdir)/%D%/arm-codegen.stamp;		\
	  $(MAKE) $(AM_MAKEFLAGS) %D%/arm-codegen.stamp;	\
	fi

src_libtc_la_SOURCES +=				\
	%D%/fwd.hh					\
  %D%/cpu.hh %D%/cpu.cc				\
  %D%/arm-layout.hh %D%/arm-layout.cc		\
  %D%/arm-assembly.hh %D%/arm-assembly.cc	\
  %D%/arm-codegen.hh %D%/arm-codegen.cc			\
  %D%/target.hh %D%/target.cc

nodist_src_libtc_la_SOURCES += %D%/runtime.cc

check_PROGRAMS += %D%/test-target
%C%_test_target_LDADD = src/libtc.la

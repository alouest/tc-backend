/**
 ** \file target/arm/fwd.hh
 ** \brief Forward declarations for target::arm items.
 */

#pragma once

namespace target::arm
{
  // arm-codegen.hh.
  class ArmCodegen;

  // arm-assembly.hh.
  class ArmAssembly;
} // namespace target::arm

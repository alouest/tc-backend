/**
 ** \file target/mips/spim-assembly.cc
 ** \brief Implementation of target::mips::SpimAssembly.
 */

#include <string>

#include <assem/libassem.hh>
#include <misc/contract.hh>
#include <target/mips/spim-assembly.hh>
#include <target/mips/spim-layout.hh>

namespace target::mips
{
  SpimAssembly::SpimAssembly()
    : Assembly(SpimLayout::instance())
  {}

  /*------------------------.
  | Arithmetic operations.  |
  `------------------------*/

  std::string
  SpimAssembly::binop_inst(const tree::Binop::Oper& op) const
  {
    std::string opcode;
    switch(op) {
      case tree::Binop::Oper::add : return "add";
      case tree::Binop::Oper::sub : return "sub";
      case tree::Binop::Oper::mul : return "mul";
      case tree::Binop::Oper::div : return "div";
      case tree::Binop::Oper::logic_and : return "and";
      case tree::Binop::Oper::logic_or : return "or";
      case tree::Binop::Oper::rshift : return "srl";
      case tree::Binop::Oper::lshift : return "sll";
      case tree::Binop::Oper::arshift : return "sra";
      case tree::Binop::Oper::logic_xor : return "xor";
      default : abort();
    }
    return opcode;

  // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::binop_build(const tree::Binop::Oper& op,
                            const temp::Temp& left,
                            const temp::Temp& right,
                            const temp::Temp& dst) const
  {
    std::string opcode = binop_inst(op) + "\t'd, 's, 's";
    return { new assem::Oper(opcode, {left, right}, {dst}) };
  }


  assem::Instrs
  SpimAssembly::binop_build(const tree::Binop::Oper& op,
                            const temp::Temp& left,
                            int right,
                            const temp::Temp& dst) const
  {
    temp::Temp tmp;
    std::string opcode;
    switch(op) {
    case tree::Binop::Oper::add :
      opcode = "addi\t'd, 's, " +  std::to_string(right);
      break;
    case tree::Binop::Oper::sub :
      opcode = "addi\t'd, 's, " +  std::to_string(right * -1);
      break;
    case tree::Binop::Oper::mul :
      opcode = "mul\t'd, 's, " + std::to_string(right);
      break;
    case tree::Binop::Oper::div : 
      opcode = "div\t'd, 's, " + std::to_string(right);
      break;
    case tree::Binop::Oper::logic_and :
      opcode = "andi\t'd, 's, " +  std::to_string(right);
      break;
    case tree::Binop::Oper::logic_or :
      opcode = "ori\t'd, 's, " +  std::to_string(right);
      break;
    case tree::Binop::Oper::rshift : 
      opcode = "srl\t'd, 's, " + std::to_string(right);
      break;
    case tree::Binop::Oper::lshift : 
      opcode = "sll\t'd, 's, " + std::to_string(right);
      break;
    case tree::Binop::Oper::arshift :
      opcode = "sra\t'd, 's, " + std::to_string(right);
      break;
    case tree::Binop::Oper::logic_xor : 
      opcode = "xori\t'd, 's, " +  std::to_string(right);
      break;
    default : abort();
    }
    
    return { new assem::Oper(opcode, {left}, {dst}) };
  // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::binop_build(const tree::Binop::Oper& op,
                            int left,
                            const temp::Temp& right,
                            const temp::Temp& res) const
  {
    return binop_build(op, right, left, res);
  }

  /*------------------.
  | Move operations.  |
  `------------------*/

  assem::Instrs
  SpimAssembly::move_build(const temp::Temp& src,
                           const temp::Temp& dst) const
  {
    return { new assem::Move("move\t'd, 's", {src}, {dst}) };
    // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::move_build(int immediate, const temp::Temp& dst) const
  {
    std::string opcode = "li\t'd, " + std::to_string(immediate);
    return { new assem::Oper(opcode, {}, { dst }) };
  }

  assem::Instrs
  SpimAssembly::move_build(const temp::Label& label,
                           const temp::Temp& dst) const
  {
    return { new assem::Oper("la\t'd, " + label.string_get(), {}, { dst }) };
  }


  /*------------------.
  | Load operations.  |
  `------------------*/

  assem::Instrs
  SpimAssembly::load_build(const temp::Temp& base,
                           int offset,
                           const temp::Temp& dst) const
  {
    return {new assem::Oper("lw\t'd, " + std::to_string(offset) + "('s)", {base}, {dst}), };
    // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::load_build(const temp::Temp& base,
                           const temp::Temp& dst) const
  {
    return { new assem::Oper("lw\t'd, ('s)", {base}, {dst}) };
  }

  assem::Instrs
  SpimAssembly::load_build(int absolute,
                           const temp::Temp& dst) const
  {
    std::string opcode = "lw\t'd, " + std::to_string(absolute);
    return { new assem::Oper(opcode, {}, { dst }) };
  }


  /*-------------------.
  | Store operations.  |
  `-------------------*/

  assem::Instrs
  SpimAssembly::store_build(const temp::Temp& src,
                            const temp::Temp& base,
                            int offset) const
  {
    std::string opcode = "sw\t's, " + std::to_string(offset) + "('s)";
    return { new assem::Oper(opcode, { src, base }, {}) };
  // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::store_build(const temp::Temp& src,
                            int absolute) const
  {
    std::string opcode = "sw\t's, " + std::to_string(absolute);
    return { new assem::Oper(opcode, { src }, {}) };
  }

  /*--------.
  | Label.  |
  `--------*/

  assem::Instrs
  SpimAssembly::label_build(const temp::Label& label) const
  {
    return { new assem::Label(label.string_get() + ":", label) };
  }

  /*-------------------.
  | Conditional jump.  |
  `-------------------*/

  std::string
  SpimAssembly::cjump_inst(const tree::Cjump::Relop& op) const
  {
    std::string opcode;
    switch(op) {
    case tree::Cjump::Relop::eq : return "beq";
    case tree::Cjump::Relop::ne : return "bne";
    case tree::Cjump::Relop::lt : return "blt";
    case tree::Cjump::Relop::gt : return "bgt";
    case tree::Cjump::Relop::le : return "ble";
    case tree::Cjump::Relop::ge : return "bge";
    case tree::Cjump::Relop::ult : return "bltu";
    case tree::Cjump::Relop::ugt : return "bgtu";
    case tree::Cjump::Relop::ule :  return "bleu";
    case tree::Cjump::Relop::uge : return "bgeu";
    default : abort();
    }
    return opcode;
  // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::cjump_build(const tree::Cjump::Relop& op,
                            const temp::Temp& left,
                            const temp::Temp& right,
                            const temp::Label& label_true,
                            const temp::Label& label_false) const
  {
    return {new assem::Oper(cjump_inst(op) + "\t's, 's, 'j", {left, right}, {}, {label_true}), };
  // FIXedME: Some code was deleted here.
  }

  assem::Instrs
  SpimAssembly::cjump_build(const tree::Cjump::Relop& op,
                            const temp::Temp& left,
                            int right,
                            const temp::Label& label_true,
                            const temp::Label& label_false) const
  {
    return {new assem::Oper(cjump_inst(op) + "\t's," + std::to_string(right)+ ", 'j", {left}, {}, {label_true}), };
    // FIXedME: Some code was deleted here.
  }


  /*---------------------.
  | Unconditional jump.  |
  `---------------------*/

  assem::Instrs
  SpimAssembly::jump_build(const temp::Label& label) const
  {
    return { new assem::Oper("j\t'j", {}, {}, { label }) };
  }

  /*-----------.
  | Function.  |
  `-----------*/

  assem::Instrs
  SpimAssembly::call_build(const temp::Label& label,
                           const temp::temp_list_type& args,
                           const temp::temp_list_type& dst) const
  {
    return { new assem::Oper("jal\t" "tc_" + label.string_get(),
                             args, dst) };
  }

  assem::Instrs
  SpimAssembly::ret_build(const temp::Temp& ret) const
  {
    return { new assem::Oper("jr\t's", { ret }, {}) };
  }
} // namespace target::mips

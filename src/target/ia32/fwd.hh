/**
 ** \file target/ia32/fwd.hh
 ** \brief Forward declarations for target::ia32 items.
 */

#pragma once

namespace target::ia32
{
  // codegen.hh.
  class Codegen;

  // gas-assembly.hh.
  class GasAssembly;
} // namespace target::ia32

/**
 ** \file ast/method-call-exp.hxx
 ** \brief Inline methods of ast::MethodCallExp.
 */

#pragma once

#include <ast/method-call-exp.hh>

namespace ast
{
  inline
  Var& MethodCallExp::obj_get()
  {
    return *obj_;
  }

  inline
  const Var& MethodCallExp::obj_get() const
  {
    return *obj_;
  }
  // FIXME: Some code was deleted here.

} // namespace ast


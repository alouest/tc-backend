/**
 ** \file ast/call-exp.hh
 ** \brief Declaration of ast::CallExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/function-dec.hh>
#include <misc/symbol.hh>

namespace ast
{

  /// CallExp.
  class CallExp : public Exp
  {
  public:
    CallExp(const Location& loc, const misc::symbol& name, const std::vector<Exp*>* arg);
    CallExp(const CallExp&) = delete;
    CallExp& operator=(const CallExp&) = delete;

    virtual ~CallExp();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    const std::vector<Exp*>* arg_get() const;
    const misc::symbol name_get() const;
    misc::symbol& name_get();
    void name_set(misc::symbol name);
    const FunctionDec* def_get() const;
    FunctionDec* def_get();
    void def_set(FunctionDec* def);

  protected:
    const std::vector<Exp*>* arg_;
    misc::symbol name_;
    FunctionDec* def_;
  // FIXME: Some code was deleted here.
  };

} // namespace ast

#include <ast/call-exp.hxx>


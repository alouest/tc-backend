/**
 ** \file ast/record-ty.hh
 ** \brief Declaration of ast::RecordTy.
 */

#pragma once

#include <ast/field.hh>
#include <ast/ty.hh>

namespace ast
{

  /// RecordTy.
  class RecordTy : public Ty
  {
    public:
    RecordTy(const Location& location, std::vector<Field*>* tyfields);
    virtual ~RecordTy();
    const std::vector<Field*> get_tyfields() const;
    std::vector<Field*> get_tyfields();
    void accept(ConstVisitor& v) const;
    void accept(Visitor& v);
    private:
    std::vector<Field*>* tyfields_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/record-ty.hxx>


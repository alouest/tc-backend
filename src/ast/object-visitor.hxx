/**
 ** \file ast/object-visitor.hxx
 ** \brief Implementation for ast/object-visitor.hh.
 */

#pragma once

#include <misc/contract.hh>
#include <ast/all.hh>
#include <ast/object-visitor.hh>

namespace ast
{

  template <template <typename> class Const>
  GenObjectVisitor<Const>::GenObjectVisitor()
    : GenVisitor<Const>()
  {}

  template <template <typename> class Const>
  GenObjectVisitor<Const>::~GenObjectVisitor()
  {}


  /*-------------------------------.
  | Object-related visit methods.  |
  `-------------------------------*/

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<ClassTy>& e)
  {
    e.super_get().accept(*this);
    // if (e.decs_get() != nullptr)
      e.decs_get().accept(*this);
  // FIXedME: Some code was deleted here.
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodDecs>& e)
  {
    for (auto i : e.decs_get())
    {
      i->accept(*this);
    }
  // FIXedME: Some code was deleted here.
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodDec>& e)
  {
    e.formals_get().accept(*this);
    if (e.result_get() != nullptr)
      e.result_get()->accept(*this);
    if (e.body_get() != nullptr)
      e.body_get()->accept(*this);
  // FIXedME: Some code was deleted here.
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodCallExp>& e)
  {
    e.obj_get().accept(*this);
    if (e.arg_get() != nullptr)
    {
      for (auto i : *(e.arg_get()))
      {
        i->accept(*this);
      }
    }
  // FIXedME: Some code was deleted here.
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<ObjectExp>& e)
  {
    e.get_id().accept(*this);
  // FIXedME: Some code was deleted here.
  }


} // namespace ast

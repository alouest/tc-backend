/**
 ** \file ast/let-exp.hxx
 ** \brief Inline methods of ast::LetExp.
 */

#pragma once

#include <ast/let-exp.hh>

namespace ast
{
  inline const DecsList& LetExp::get_decs() const
  {
    return *decs_;
  }
  inline DecsList& LetExp::get_decs()
  {
    return *decs_;
  }
  inline const Exp& LetExp::get_exps() const
  {
    return *exps_;
  }
  inline Exp& LetExp::get_exps()
  {
    return *exps_;
  }

  // FIXedME: Some code was deleted here.

} // namespace ast


/**
 ** \file ast/array-exp.cc
 ** \brief Implementation of ast::ArrayExp.
 */

#include <ast/visitor.hh>
#include <ast/array-exp.hh>

namespace ast
{
  ArrayExp::ArrayExp(const Location& loc, Exp* row, Exp* of, NameTy* name)
    : Exp(loc)
    , row_(row)
    , of_(of)
    , name_(name)
  {}

  ArrayExp::~ArrayExp()
  {
    delete (row_);
    delete (of_);
    delete (name_);
  }
  
  void ArrayExp::accept(Visitor& v)
  {
    v(*this);
  }

  void ArrayExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  
  // FIXMEed: Some code was deleted here.

} // namespace ast


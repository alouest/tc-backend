/**
 ** \file ast/record-ty.hxx
 ** \brief Inline methods of ast::RecordTy.
 */

#pragma once

#include <ast/record-ty.hh>

namespace ast
{
  inline const std::vector<Field*> RecordTy::get_tyfields() const
  {
    return *tyfields_;
  }

  inline std::vector<Field*> RecordTy::get_tyfields()
  {
    return *tyfields_;
  }

  // FIXME: Some code was deleted here.

} // namespace ast


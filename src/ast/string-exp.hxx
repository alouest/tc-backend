/**
 ** \file ast/string-exp.hxx
 ** \brief Inline methods of ast::StringExp.
 */

#pragma once

#include <ast/string-exp.hh>

namespace ast
{
  inline const std::string* StringExp::get_string()
  {
    return string_;
  }
  inline const std::string* StringExp::get_string() const
  {
    return string_;
  }

  // FIXedME: Some code was deleted here.

} // namespace ast


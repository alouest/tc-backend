/**
 ** \file ast/typable.cc
 ** \brief Implementation of ast::Typable.
 */

#include <ast/visitor.hh>
#include <ast/typable.hh>

namespace ast
{
  Typable::Typable()
  {
    type_ = nullptr;
  }
  Typable::~Typable()
  {}
    
  // type::Type* Typable::type_get()
  // {
  //   return type_;
  // }
  const type::Type* Typable::type_get() const
  {
    return type_;
  }
  
  void Typable::type_set(const type::Type* type)
  {
    type_ = type;
  }
  
  // FIXME: Some code was deleted here.

} // namespace ast


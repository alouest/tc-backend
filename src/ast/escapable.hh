/**
 ** \file ast/escapable.hh
 ** \brief Declaration of ast::Escapable.
 */

#pragma once

#include <ast/fwd.hh>

namespace ast
{

  /// Escapable.
  class Escapable
  {
  public:
    Escapable();
    Escapable(const ast::Escapable&) = delete;
    void operator=(const ast::Escapable&) = delete;
    
    ~Escapable();

    int depth_get();
    const int depth_get() const;
    void depth_set(int depth);
    void escaped_set(bool escaped);
    bool escaped_get();
    const bool escaped_get() const;
    bool used_get();
    void used_set();
  protected:
    int depth_;
    bool escaped_;
    bool used_;
  // FIXME: Some code was deleted here.
  };

} // namespace ast

#include <ast/escapable.hxx>


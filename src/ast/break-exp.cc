/**
 ** \file ast/break-exp.cc
 ** \brief Implementation of ast::BreakExp.
 */

#include <ast/visitor.hh>
#include <ast/break-exp.hh>

namespace ast
{
  BreakExp::BreakExp(const Location& loc)
    : Exp(loc)
    , def_(nullptr)
  {}

  BreakExp::~BreakExp()
  {}

  void BreakExp::accept(Visitor& v)
  {
    v(*this);
  }

  void BreakExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  // FIXME: Some code was deleted here.

} // namespace ast


/**
 ** \file ast/array-exp.hxx
 ** \brief Inline methods of ast::ArrayExp.
 */

#pragma once

#include <ast/array-exp.hh>

namespace ast
{
  inline
  const Exp& ArrayExp::row_get() const
  {
    return *row_;
  }
  
  inline
  Exp& ArrayExp::row_get()
  {
    return *row_;
  }
  
  inline
  const Exp& ArrayExp::of_get() const
  {
    return *of_;
  }
  
  inline
  Exp& ArrayExp::of_get()
  {
    return *of_;
  }
  
  inline
  const NameTy& ArrayExp::name_get() const
  {
    return *name_;
  }

  inline
  NameTy& ArrayExp::name_get()
  {
    return *name_;
  }
  
  // FIXMEed: Some code was deleted here.

} // namespace ast


/**
 ** \file ast/call-exp.cc
 ** \brief Implementation of ast::CallExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/call-exp.hh>

namespace ast
{
  CallExp::CallExp(const Location& loc, const misc::symbol& name,
    const std::vector<Exp*>* arg)
    : Exp(loc)
    , name_(name)
    , arg_(arg)
    , def_(nullptr)
  {}

  CallExp::~CallExp()
  {
    if (arg_ != nullptr)
      {
	for (auto i : *arg_)
	  delete i;
	delete (arg_);
      }
  }
  
  void CallExp::accept(Visitor& v)
  {
    v(*this);
  }

  void CallExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  // FIXME: Some code was deleted here.

} // namespace ast


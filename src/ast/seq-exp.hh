/**
 ** \file ast/seq-exp.hh
 ** \brief Declaration of ast::SeqExp.
 */

#pragma once

#include <ast/exp.hh>

namespace ast
{

  /// SeqExp.
  class SeqExp : public Exp
  {
    public:
    SeqExp(const Location& location, std::vector<Exp*>* exps);
    virtual ~SeqExp();
    SeqExp(const SeqExp&) = delete;
    SeqExp& operator=(const SeqExp&) = delete;
    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;
    std::vector<Exp*>& get_exps();
    const std::vector<Exp*>& get_exps() const;

    protected:
    std::vector<Exp*>* exps_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/seq-exp.hxx>


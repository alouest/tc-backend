/**
 ** \file ast/string-exp.hh
 ** \brief Declaration of ast::StringExp.
 */

#pragma once

#include <ast/exp.hh>
#include <string>

namespace ast
{

  /// StringExp.
  class StringExp : public Exp
  {
    public:
    StringExp(const Location& location, const std::string& string);
    ~StringExp();
    StringExp(const StringExp&) = delete;
    StringExp& operator=(const StringExp&) = delete;
    void accept(Visitor& v) override;
    void accept(ConstVisitor& v) const override;
    const std::string* get_string();
    const std::string* get_string() const;
    private:
    const std::string* string_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/string-exp.hxx>


/**
 ** \file ast/call-exp.hxx
 ** \brief Inline methods of ast::CallExp.
0 */

#pragma once

#include <ast/call-exp.hh>

namespace ast
{
  inline
  const std::vector<Exp*>* CallExp::arg_get() const
    {
      return arg_;
    }
  
  inline
  const misc::symbol CallExp::name_get() const
    {
      return name_;
    }
  
  inline
  misc::symbol& CallExp::name_get()
    {
      return name_;
    }

  inline
  void CallExp::name_set(misc::symbol name)
    {
      name_ = name;
    }

  inline
  const FunctionDec* CallExp::def_get() const
  {
    return def_;
  }

  inline
  FunctionDec* CallExp::def_get()
  {
    return def_;
  }

  inline
  void CallExp::def_set(FunctionDec* def)
  {
    def_ = def;
  }
  // FIXME: Some code was deleted here.

} // namespace ast


/**
 ** \file ast/object-exp.hxx
 ** \brief Inline methods of ast::ObjectExp.
 */

#pragma once

#include <ast/object-exp.hh>

namespace ast
{
    inline const NameTy& ObjectExp::get_id() const
    {
      return *id_;
    }
    inline NameTy& ObjectExp::get_id()
    {
      return *id_;
    }
  // FIXME: Some code was deleted here.

} // namespace ast


/**
 ** \file ast/pretty-printer.cc
 ** \brief Implementation of ast::PrettyPrinter.
 */

#include <misc/escape.hh>
#include <misc/indent.hh>
#include <misc/separator.hh>
#include <misc/pair.hh>
#include <ast/all.hh>
#include <ast/pretty-printer.hh>
#include <ast/libast.hh>

#include <type/class.hh>

namespace ast
{

  // Anonymous namespace: these functions are private to this file.
  namespace
  {
    /// Output \a e on \a ostr.
    inline
    std::ostream&
    operator<<(std::ostream& ostr, const Escapable& e)
    {
      if (escapes_display(ostr) && e.escaped_get()
  // FIXME: Some code was deleted here.
          )
        ostr << "/* escaping */ ";

      return ostr;
    }

    /// \brief Output \a e on \a ostr.
    ///
    /// Used to factor the output of the name declared,
    /// and its possible additional attributes.
    inline
    std::ostream&
    operator<<(std::ostream& ostr, const Dec& e)
    {
      ostr << e.name_get();
      if (bindings_display(ostr))
        ostr << " /* " << &e << " */";
      return ostr;
    }
  }

  PrettyPrinter::PrettyPrinter(std::ostream& ostr)
    : ostr_(ostr)
  {}

  /* Foo[10]. */

  void PrettyPrinter::operator()(const FunctionDec& e)
  {
    if (e.body_get() != nullptr)
      ostr_ << "function ";
    else
      ostr_ << "primitive ";
    ostr_ << e;
    ostr_ << "(";
    for (auto i : e.formals_get().decs_get())
    {
      if (i != *(e.formals_get().decs_get().begin()))
        ostr_ << ", ";
      i->accept(*this);
    }
    ostr_ << " )";
    if (e.result_get() != nullptr)
    {
      ostr_ << ": ";
      e.result_get()->accept(*this);
    }
    if (e.body_get() != nullptr)
    {
      ostr_ << " = ";
      ostr_  << misc::incendl;
      e.body_get()->accept(*this);
      ostr_ << misc::decendl;
    }
    else
    {
      ostr_ << misc::iendl;
    }
  }

  void PrettyPrinter::operator()(const MethodDec& e)
  {
    ostr_ << "method " << e << " (";
    for (auto i: e.formals_get().decs_get())
    {
     if (i != *(e.formals_get().decs_get().begin()))
        ostr_ << ", ";
      i->accept(*this);
    }
    ostr_ << ")";
    if (e.result_get() != nullptr)
    {
      ostr_ << ": ";
      e.result_get()->accept(*this);
    }
    if (e.body_get() != nullptr)
    {
      ostr_ << " = " << misc::iendl;
      ostr_ << "("  << misc::incendl;
      e.body_get()->accept(*this);
      ostr_ << ")" << misc::decindent;
    }
    ostr_ << misc::iendl;
  }
  void PrettyPrinter::operator()(const TypeDec& e)
  {
    ostr_ << "type " << e;
    ostr_ << " = ";
    e.ty_get().accept(*this);
    ostr_ << misc::iendl;
  }
  
  void PrettyPrinter::operator()(const VarDec& e)
  {
    if (escapes_display(ostr_) && e.escaped_get())
      ostr_ << "/* escaping */ ";
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << &e << " */";
    if (e.type_name_get() != nullptr)
    {
      ostr_ << " : ";
      e.type_name_get()->accept(*this);
    }
    if (e.init_get() != nullptr)
    {
      ostr_ << " := ";
      e.init_get()->accept(*this);
      ostr_ << misc::iendl;
    }
  }

  void PrettyPrinter::operator()(const VarDecs& e)
  {
    for (auto i : e.decs_get())
    {
      ostr_ << "var ";
      i->accept(*this);
      ostr_ << misc::iendl;
    }
  }

  void PrettyPrinter::PrettyPrinter::operator()(const CastVar& e)
  {
    ostr_ << "_cast(" << e.var_get() << ", " << e.ty_get() << ')';
  }

  void PrettyPrinter::PrettyPrinter::operator()(const FieldVar& e)
  {
    e.var_get().accept(*this);
    ostr_ << "." << e.name_get();
  // FIXedME: Some code was deleted here.
  }

  void PrettyPrinter::PrettyPrinter::operator()(const SimpleVar& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << e.def_get() << " */";
  }

  void PrettyPrinter::PrettyPrinter::operator()(const SubscriptVar& e)
  {
    ostr_ << e.var_get() << '[' << misc::incindent << e.index_get()
          << misc::decindent << ']';
  }

  void PrettyPrinter::operator()(const ArrayExp& e)
  {
    e.name_get().accept(*this);
    ostr_ << "[";
    e.row_get().accept(*this);
    ostr_ << "] of ";
    e.of_get().accept(*this);
  }

  void PrettyPrinter::operator()(const AssignExp& e)
  {
    e.var_get().accept(*this);
    ostr_ << " := ";
    e.val_get().accept(*this);
  }

  void PrettyPrinter::operator()(const BreakExp& e)
  {
    ostr_ << "break";
    if (bindings_display(ostr_))
      ostr_ << " /* " << e.def_get() << " */";
  }

  void PrettyPrinter::operator()(const CallExp& e)
  {
    ostr_ << e.name_get();
      if (bindings_display(ostr_))
        ostr_ << " /* " << e.def_get() << " */";
    ostr_ << "(";
    if (e.arg_get() != nullptr)
    {
      for (auto i : *(e.arg_get()))
      {
        if (i != *(e.arg_get()->begin()))
          ostr_ << ", ";
        i->accept(*this);
      }
    }
    ostr_ << ")" << misc::iendl;
  }

  void PrettyPrinter::operator()(const MethodCallExp& e)
  {
    e.obj_get().accept(*this);
    // if (bindings_display(ostr_))
    //   ostr_ << " /* " << e.def_get() << " */";
    ostr_ << "." << e.name_get() << "(";
    if (e.arg_get() != nullptr)
      {
	for (auto i : *(e.arg_get()))
	  {
	    if (i != *(e.arg_get()->begin()))
	      ostr_ << ", ";
	    i->accept(*this);
	  }
      }
    ostr_ << ")";
  }
  
  void PrettyPrinter::PrettyPrinter::operator()(const CastExp& e)
  {
    ostr_ << "_cast(" << e.exp_get() << ", " << e.ty_get() << ')';
  }
  void PrettyPrinter::operator()(const ForExp& e)
  {
    ostr_ << "for ";
    if (bindings_display(ostr_))
      ostr_ << " /* " << &e << " */";
    e.vardec_get().accept(*this);
    ostr_ << " to ";
    e.hi_get().accept(*this);
    ostr_ << " do" << misc::incendl;
    e.body_get().accept(*this);
    ostr_ << misc::decendl;
  }

  void PrettyPrinter::operator()(const IfExp& e)
  {
    ostr_ << "(if ";
    e.get_if().accept(*this);
    ostr_ << misc::iendl;
    // ostr_ << ")" << misc::iendl;
    ostr_ << "then" << misc::incendl;
    // ostr_ << "(" << misc::incendl;
    e.get_then().accept(*this);
    ostr_ << misc::decendl;
    // ostr_ << ")" << misc::iendl;
    ostr_ << "else"  << misc::incendl;
    // ostr_ << "(" << misc::incendl;
    if (e.get_else() != nullptr)
      e.get_else()->accept(*this);
    else
      ostr_ << "()";
    ostr_ << ")" << misc::decindent;
    // ostr_ << ")" << misc::decendl;
  }

  void PrettyPrinter::operator()(const IntExp& e)
  {
    ostr_ << e.value_get();
  }

  void PrettyPrinter::operator()(const LetExp& e)
  {
    ostr_ << "let" << misc::incendl;
    e.get_decs().accept(*this);
    ostr_ << misc::decendl << "in" << misc::incendl;
    e.get_exps().accept(*this);
    ostr_ << misc::decendl << "end" << misc::iendl;
  }

  void PrettyPrinter::operator()(const NilExp& e)
  {
    ostr_ << "nil";
  }

  void PrettyPrinter::operator()(const ObjectExp& e)
  {
    ostr_ << "new ";
    e.get_id().accept(*this);
    // if (bindings_display(ostr_))
    //   ostr_ << " /*" << e.def_get() << " */";

  }

  void PrettyPrinter::operator()(const OpExp& e)
  {
    ostr_ << "(";
    e.left_get().accept(*this);
    ostr_ << str(e.oper_get());
    e.right_get().accept(*this);
    ostr_ << ")";
  }

  void PrettyPrinter::operator()(const RecordExp& e)
  {
    e.get_type().accept(*this);
    ostr_ << " {";
    for (auto i : e.get_fields())
    {
      if (i != *(e.get_fields().begin()))
        ostr_ << ", ";
      i->accept(*this);
    }
    ostr_ << " }";
  }

  void PrettyPrinter::operator()(const SeqExp& e)
  {
    if (e.get_exps().size() == 0)
      {
	ostr_ << "()";
	return;
      }
    if (e.get_exps().size() != 1)
      ostr_ << "(" << misc::incindent;
    for (auto i : e.get_exps())
    {
      if (i != *(e.get_exps().begin()))
        ostr_ << misc::iendl;
      i->accept(*this);
      if (i != *(e.get_exps().end() - 1))
        ostr_ << ";";
    }
    if (e.get_exps().size() != 1)
      ostr_ << misc::decendl << ")";
  }

  void PrettyPrinter::operator()(const StringExp& e)
  {
    ostr_ << "\"" << misc::escape(*(e.get_string())) << "\"";
  }

  void PrettyPrinter::operator()(const WhileExp& e)
  {
    ostr_ << "while ";
    if (bindings_display(ostr_))
      ostr_ << " /* " << &e << " */";
    e.test_get().accept(*this);
    ostr_ << " do" << misc::incendl;
    e.body_get().accept(*this);
    ostr_ << misc::decindent;
  }

  void PrettyPrinter::operator()(const ArrayTy& e)
  {
    ostr_ << "array of ";
    e.base_type_get().accept(*this);
  }

  void PrettyPrinter::operator()(const ClassTy& e)
  {
    ostr_ << "class extends ";
    e.super_get().accept(*this);
    ostr_ << "{";
    e.decs_get().accept(*this);
    ostr_ << "}";
    ostr_ << misc::iendl;
  }

  void PrettyPrinter::operator()(const NameTy& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " <<  e.def_get() << " */";
  }

  void PrettyPrinter::operator()(const RecordTy& e)
  {
    ostr_ << "{";
    for (auto i : e.get_tyfields())
    {
      if (i != *(e.get_tyfields().begin()))
        ostr_ << ", ";
      i->accept(*this);
    }
    ostr_ << "}";
  }

  void PrettyPrinter::operator()(const Field& e)
  {
    ostr_ << e.name_get() << " : ";
    e.type_name_get().accept(*this);
  }

  void PrettyPrinter::operator()(const FieldInit& e)
  {
    ostr_ << e.name_get() << " = ";
    e.init_get().accept(*this);
  }
  // FIXedME: Some code was deleted here.

} // namespace ast

/**
 ** \file ast/record-exp.cc
 ** \brief Implementation of ast::RecordExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/record-exp.hh>

namespace ast
{
  RecordExp::RecordExp(const Location& location, NameTy* type, std::vector<FieldInit*>* fields)
  : Exp(location)
  , type_(type)
  , fields_(fields)
  {}
  RecordExp::~RecordExp()
  {
    for (auto i : *fields_)
      delete i;
    delete type_;
    delete fields_;
  }
  void RecordExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  void RecordExp::accept(Visitor& v)
  {
    v(*this);
  }

  // FIXedME: Some code was deleted here.

} // namespace ast


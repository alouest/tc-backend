/**
 ** \file ast/assign-exp.cc
 ** \brief Implementation of ast::AssignExp.
 */

#include <ast/visitor.hh>
#include <ast/assign-exp.hh>

namespace ast
{
  AssignExp::AssignExp(const Location& loc, Var* var, Exp* val)
    : Exp(loc)
    , val_(val)
    , var_(var)
  {}

  AssignExp::~AssignExp()
  {
    delete (var_);
    delete (val_);
  }
  
  void AssignExp::accept(Visitor& v)
  {
    v(*this);
  }

  void AssignExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  // FIXMEed: Some code was deleted here.

} // namespace ast


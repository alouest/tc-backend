/**
 ** \file ast/escapable.cc
 ** \brief Implementation of ast::Escapable.
 */

#include <ast/visitor.hh>
#include <ast/escapable.hh>

namespace ast
{
  Escapable::Escapable()
  {
    depth_ = 0;
    escaped_ = true;
    used_ = false;
  }

  Escapable::~Escapable()
  {}
  // FIXME: Some code was deleted here.

} // namespace ast


/**
 ** \file escapes/escapes-visitor.cc
 ** \brief Implementation for escapes/escapes-visitor.hh.
 */

#include <ast/all.hh>
#include <escapes/escapes-visitor.hh>
#include <misc/contract.hh>

namespace escapes
{
  void EscapesVisitor::operator()(ast::VarDec& e)
  {
    e.depth_set(depth_);
    e.escaped_set(false);
    if (e.init_get() != nullptr)
      e.init_get()->accept(*this);
  }
  
  void EscapesVisitor::operator()(ast::SimpleVar& e)
  {
    if (std::getenv("DEBUG"))
      std::cout << e.name_get() << " : " << e.def_get() << std::endl;
    if (e.def_get() != nullptr)
      {
	e.def_get()->escaped_set((e.def_get()->depth_get() != depth_) &&
			       (e.def_get()->escaped_get() || !e.def_get()->used_get()));
	e.def_get()->used_set();
      }
  }

  void EscapesVisitor::operator()(ast::FunctionDec& e)
  {
    depth_++;
    for (auto i : e.formals_get().decs_get())
      i->accept(*this);
    if (e.body_get() == nullptr)
      for (auto i : e.formals_get().decs_get())
	i->escaped_set(false);

    if (e.result_get() != nullptr)
      e.result_get()->accept(*this);
    if (e.body_get() != nullptr)
      e.body_get()->accept(*this);
    depth_--;
  }
  
  void EscapesVisitor::operator()(ast::LetExp& e)
  {
    depth_++;
    e.get_decs().accept(*this);
    e.get_exps().accept(*this);
    depth_--;
  }
  
  void EscapesVisitor::operator()(ast::MethodDec& e)
  {
    depth_++;
    for (auto i : e.formals_get().decs_get())
      i->accept(*this);
    if (e.result_get() != nullptr)
      e.result_get()->accept(*this);
    if (e.body_get() != nullptr)
      e.body_get()->accept(*this);
    depth_--;
    
  }
  
  // FIXME: Some code was deleted here.

} // namespace escapes

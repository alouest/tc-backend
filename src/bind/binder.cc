/**
 ** \file bind/binder.cc
 ** \brief Implementation for bind/binder.hh.
 */

#include <ast/all.hh>
#include <bind/binder.hh>

#include <misc/contract.hh>

namespace bind
{

  Binder::Binder()
    : loop_(new std::stack<ast::Exp*>())
  {}

  Binder::~Binder()
  {
    delete loop_;
  }
  /*-----------------.
  | Error handling.  |
  `-----------------*/

  /// The error handler.
  const misc::error&
  Binder::error_get() const
  {
    return error_;
  }

  // FIXME: Some code was deleted here (Error reporting).

  void
  Binder::check_main(const ast::FunctionDec& e)
  {
    if (e.name_get() == "_main")
      {
	main_++;
	if (main_ > 1)
	  error_ << misc::error::error_type::bind << e.location_get() <<
	    ": redefinition of: " << e.name_get() << std::endl;
      }
  // FIXedME: Some code was deleted here.
  }

  /*----------------.
  | Symbol tables.  |
  `----------------*/

  bool
  Binder::is_primitive(misc::symbol sym)
  {
    return (sym == "int" || sym == "string");
  }
  
  void
  Binder::scope_begin()
  {
    function_.scope_begin();
    var_.scope_begin();
    type_.scope_begin();
  // FIXME: Some code was deleted here.
  }

  void
  Binder::scope_end()
  {
    function_.scope_end();
    var_.scope_end();
    type_.scope_end();
  // FIXME: Some code was deleted here.
  }

  /*---------.
  | Visits.  |
  `---------*/

  // FIXME: Some code was deleted here.

  void
  Binder::operator()(ast::LetExp& e)
  {
    scope_begin();
    outside_ = true;
    e.get_decs().accept(*this);
    e.get_exps().accept(*this);
    outside_ = false;
    scope_end();
  // FIXedME: Some code was deleted here.
  }

  void
  Binder::operator()(ast::WhileExp& e)
  {
    scope_begin();
    loop_->push(&e);
    outside_ = true;
    e.test_get().accept(*this);
    outside_ = false;
    e.body_get().accept(*this);
    loop_->pop();
    scope_end();
  }
  
  void
  Binder::operator()(ast::ForExp& e)
  {
    scope_begin();
    loop_->push(&e);
    outside_ = true;
    e.hi_get().accept(*this);
    var_.put(e.vardec_get().name_get(), &(e.vardec_get()));
    e.vardec_get().accept(*this);
    outside_ = false;
    e.body_get().accept(*this);
    loop_->pop();
    scope_end();
  }
  
  void
  Binder::operator()(ast::SimpleVar& e)
  {

    if (std::getenv("DEBUG"))
      std::cerr << "I bind : " << e.name_get() << std::endl;
    auto id = e.name_get();
    e.def_set(var_.get(id));
    if (e.def_get() == nullptr)
      undeclared(e.name_get(), e);
  }

  void
  Binder::operator()(ast::CallExp& e)
  {
    auto id = e.name_get();
    e.def_set(function_.get(id));
    if (e.def_get() == nullptr)
      undeclared(e.name_get(), e);
    if (e.arg_get() != nullptr)
      for (auto i : *e.arg_get())
	i->accept(*this);
  }
  
  void
  Binder::operator()(ast::TypeDec& e)
  {
    //    type_.put(e.name_get(), &e);
    e.ty_get().accept(*this);
  }

  void
  Binder::operator()(ast::VarDecs& e)
  {
    decs_visit(e);
  }

  void
  Binder::operator()(ast::TypeDecs& e)
  {
    decs_visit(e);
  }

  void
  Binder::operator()(ast::FunctionDecs& e)
  {
    decs_visit(e);
  }

  void
  Binder::operator()(ast::FunctionDec& e)
  {
    check_main(e);
    //    function_.put(e.name_get(), &e);
    scope_begin();
    e.formals_get().accept(*this);    
    if (e.result_get() != nullptr)
      e.result_get()->accept(*this);
    outside_ = true;
    if (e.body_get() != nullptr)
      e.body_get()->accept(*this);
    scope_end();
  }

  void
  Binder::operator()(ast::VarDec& e)
  {
    //    var_.put(e.name_get(), &e);
    if (e.type_name_get() != nullptr)
      e.type_name_get()->accept(*this);
    if (e.init_get() != nullptr)
      e.init_get()->accept(*this);
  }

  void
  Binder::operator()(ast::NameTy& e)
  {
    auto id = e.name_get();
    e.def_set(type_.get(id));
    if (!is_primitive(id) && !e.def_get())
      undeclared(e.name_get(), e);
  }

  void Binder::operator()(ast::BreakExp& e)
  {
    if (loop_ == nullptr || loop_->empty() || outside_)
      break_error(e.location_get());
    else
      e.def_set(loop_->top());

  }
    
  /*-------------------.
  | Visiting VarDecs.  |
  `-------------------*/

  // FIXME: Some code was deleted here.


  /*------------------------.
  | Visiting FunctionDecs.  |
  `------------------------*/

  // FIXME: Some code was deleted here.

  /*--------------------.
  | Visiting TypeDecs.  |
  `--------------------*/
  // FIXME: Some code was deleted here.

} // namespace bind

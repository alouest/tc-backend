/**
 ** \file bind/libbind.cc
 ** \brief Define exported bind functions.
 */
#include <bind/libbind.hh>

namespace bind
{
  bool bind_object;
  
  const misc::error&
  bind(ast::Ast& program)
  {
    Binder bind;
    program.accept(bind);
    return bind.error_get();
  }
  
}
  // FIXME: Some code was deleted here.

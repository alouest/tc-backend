/**
 ** \file bind/tasks.cc
 ** \brief Bind module tasks implementation.
 */
#include <common.hh>

#define DEFINE_TASKS 1
#include <bind/tasks.hh>
#undef DEFINE_TASKS

namespace ast
{
  namespace tasks
  {
    extern std::unique_ptr<ast::DecsList> the_program;
  }
}

namespace bind
{
  namespace tasks
  {
    void bind()
    {
      auto bind = ::bind::Binder();
      ast::tasks::the_program->accept(bind);
      task_error() << bind.error_get();
      task_error().exit_on_error();
    }

    void bindings_compute()
    {
      bind();
    }

    void binding_display()
    {
      ::ast::bindings_display(std::cout) = true;
    }

    void rename()
    {
      auto rename = ::bind::Renamer();
      ::ast::tasks::the_program->accept(rename);
    }
  }
}
  // FIXedME: Some code was deleted here.

/**
 ** \file canon/libcanon.cc
 ** \brief Simplify HIR into LIR.
 */

#include <canon/canon.hh>
#include <canon/libcanon.hh>
#include <canon/traces.hh>
#include <tree/fragment.hh>
#include <tree/fragments.hh>

namespace canon
{

  void
  canonicalize(tree::Fragments& fragments, bool canon_trace_p)
  {
    class CanonVisitor : public tree::Visitor
    {
    public:
      using tree::Visitor::operator();

      CanonVisitor(bool trace_p)
        : canon_(trace_p)
      {}

      virtual void operator()(tree::ProcFrag& frag)
      {
        frag.body_set(canon_(frag.body_get()));
      }

    private:
      canon::Canon canon_;
    };

    CanonVisitor visitor(canon_trace_p);
    visitor(fragments);
  }


  void
  make_traces(tree::Fragments& fragments, bool trace_trace_p)
  {
    class TraceVisitor : public tree::Visitor
    {
    public:
      using tree::Visitor::operator();

      TraceVisitor(bool trace_p)
        : trace_(trace_p)
      {}

      virtual void operator()(tree::ProcFrag& frag)
      {
        frag.body_set(trace_(frag.body_get()));
      }

    private:
      canon::Traces trace_;
    };
    
    TraceVisitor visitor(trace_trace_p);
    visitor(fragments);
  // FIXME: Some code was deleted here (Invoke Traces on each ProcFrag).
  }

} // namespace canon

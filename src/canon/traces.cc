/**
 ** \file canon/traces.cc
 ** \brief Composing traces from canon::BasicBlock's.
 */

#include <iostream>
#include <map>
#include <algorithm>
#include <functional>

#include <misc/contract.hh>
#include <misc/indent.hh>
#include <misc/vector.hh>
#include <temp/fwd.hh>
#include <temp/identifier.hh>
#include <temp/label.hh>
#include <tree/all.hh>
#include <canon/traces.hh>

namespace canon
{

  /*---------.
  | Traces.  |
  `---------*/

  Traces::Traces(bool trace_p)
    : trace_p_(trace_p)
  {}


  Traces::block_map
  Traces::make_basic_blocks(const tree::rStm& tree,
                            const temp::Label& entry_label,
                            const temp::Label& epilogue_label)
  {
    using namespace tree;

    /* Create the basic blocks from TREE, then store them in a map.  */
    block_map blocks;
    bool block = false;
    tree_list_type block_vect;
    tree->children_get().emplace(tree->children_get().begin(),
         new tree::Label(entry_label));


    
    for (auto i = tree->children_get().begin(); i < tree->children_get().end(); i++)
    {
      if (i->is_a<tree::Label>())
      {
	if (block) {    
          block_vect.emplace_back(new tree::Jump(new tree::Name(
				  i->cast<tree::Label>()->label_get())));
	  auto bb = new BasicBlock(block_vect.begin(), block_vect.end());
	  blocks.insert_or_assign(block_vect.begin()->cast<tree::Label>()->label_get(),
				  std::unique_ptr<BasicBlock>(bb));
	}
	block_vect.clear();
	block_vect.emplace_back(*i);
	block = true;  
      }
      else if (i->is_a<tree::Jump>() ||
	       i->is_a<tree::Cjump>())
      {
	if (!block_vect.begin()->is_a<tree::Label>() || block_vect.size() == 0)
	{
	  temp::Label tmp = temp::Label();
	  block_vect.emplace(block_vect.begin(), new tree::Label(tmp));
	}
	block_vect.emplace_back(*i);
	auto bb = new BasicBlock(block_vect.begin(), block_vect.end());
	// auto warning = *block_vect.begin()->cast<tree::Label>();
	blocks.insert_or_assign(block_vect.begin()->cast<tree::Label>()->label_get(),
				std::unique_ptr<BasicBlock>(bb));
	block_vect.clear();
	block = false;
      }
      else
      {
	block_vect.emplace_back(*i);
      }
    }
    
    if (block) //finishing last elements that where not flushed
    {
      if (!block_vect.begin()->is_a<tree::Label>())
      {
	temp::Label tmp = temp::Label();
	block_vect.emplace(block_vect.begin(), new tree::Label(tmp));
      }
      
      if (!(block_vect.end() -1)->is_a<tree::Jump>())
      {
	block_vect.emplace_back(new tree::Jump(new tree::Name(epilogue_label)));
      }
      
      blocks.insert_or_assign(block_vect.begin()->cast<tree::Label>()->label_get(),
		      std::unique_ptr<BasicBlock>(new BasicBlock(block_vect.begin(),
  		     block_vect.end())));
    }
    return blocks;
  }

  
  tree::tree_list_type
  Traces::make_trace(block_map& blocks,
                     const temp::Label& entry_label,
                     const temp::Label& epilogue_label)
  {
    tree::tree_list_type res;

    std::map<std::unique_ptr<BasicBlock>*, bool> marked;
    for (auto i = blocks.begin(); i != blocks.end(); i++)
    {
      marked.insert_or_assign(&(i->second), false);
    }

    auto start = blocks.find(entry_label);
    
    while (blocks.size() != 0)
    {
      std::vector<BasicBlock> trace;
      auto b = start;
      while (!(marked.find(&(b->second))->second))
      {
        trace.emplace_back((*b->second));
        marked.find(&b->second)->second = true;
        auto out = b->second->labels_out_get();
	int num_out = 0;
        for (auto j = out.rbegin(); j != out.rend(); j++)
        {
	  num_out++;
          auto block_j = blocks.find(*j);
          if (block_j == blocks.end()) {
            continue;
          }
          auto trace_j = marked.find(&block_j->second);
          if (trace_j->second) {
            continue;
          }
          auto next = blocks.find(*j);
          if (next != blocks.end()) {
	    if (num_out == 2)
	      (trace.rbegin()->trees_get().end() - 1)->cast<tree::Cjump>()->flip();
	    blocks.erase(b);
	    b = next;
	    break;
	  }
        }
      }
      blocks.erase(b);
      for (auto k = trace.begin(); k != trace.end(); k++)
      {
        for(auto trees: (*k).trees_get()) {
            res.emplace_back(trees);
        }
      }
      auto next = blocks.begin();
      for (; next != blocks.end(); next++)
      {
	if (next->second->labels_out_get().size() == 2)
	{
	  int used = 0;
	  for (auto out: next->second->labels_out_get())
	  {
	    if (blocks.find(out) == blocks.end())
	      used++;
	  }
	  if (used >= 1)
	  {
	    start = next;
	    break;
	  }
	}
      }
      if (next != blocks.end())
	continue;
      
      
      next = blocks.begin();
      for (; next != blocks.end(); next++)
      {
	if (next->second->labels_out_get().size() == 2)
	  break;
      }
      if (next != blocks.end())
	start = next;
      else
	start = blocks.begin();
    }
    res.emplace_back(new tree::Label(epilogue_label));
    return res;
  }

  
  /*-------------------------.
  | Removing useless jumps.  |
  `-------------------------*/

  /// Is \a tree a JUMP(NAME())?  This is a predicate.
  struct jump_name_p
  {
  // FIXME: Some code was deleted here.
  };


  bool
  Traces::useless_jump_p(tree::tree_list_type::iterator begin,
                         tree::tree_list_type::iterator end)
  {
    if (end->is_a<tree::Label>() && (end->cast<tree::Label>()->label_get() == begin->cast<tree::Jump>()->label_get()[0]))
      return true;

    return false;
  // FIXME: Some code was deleted here.
  }

  void
  Traces::strip_useless_jumps(tree::tree_list_type& trace)
  {
    for (auto i = trace.begin(); i < trace.end(); i++)
    {
      if (i->is_a<tree::Jump>() && useless_jump_p(i, i + 1))
      {
        trace.erase(i);
      }
    }
  // FIXME: Some code was deleted here.
  }


  tree::rStm
  Traces::operator()(const tree::rStm& tree)
  {
    // 2. Basic Blocks.
    temp::Label entry_label;
    temp::Label epilogue_label;
    block_map basic_blocks = make_basic_blocks(tree,
                                               entry_label, epilogue_label);

    if (trace_p_)
      {
        std::cerr << "-------------------- Basic blocks ----------------------\n";
        for (block_map::value_type& b : basic_blocks)
          std::cerr << b.first << misc::incendl
                    << *b.second << misc::decendl;
        std::cerr << "--------------------------------------------------------\n";
      }
    
    
    //    3. Making the trace.
    tree::tree_list_type trace = make_trace(basic_blocks,
                                            entry_label, epilogue_label);

    if (trace_p_)
      std::cerr << "------------------------- Trace -------------------------\n"
                << trace << '\n'
                << "---------------------------------------------------------\n";

    // 4. Remove useless JUMPS.
           strip_useless_jumps(trace);

    if (trace_p_)
      std::cerr << "--------- Trace with Useless Jumps Simplified -----------\n"
                << trace << '\n'
                << "---------------------------------------------------------\n";

    return new tree::Seq(trace);
  }

} // namespace canon

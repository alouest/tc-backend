/** \file misc/scoped-map.hxx
 ** \brief Implementation of misc::scoped_map.
 */

#pragma once

#include <sstream>
#include <stdexcept>
#include <type_traits>

#include <misc/algorithm.hh>
#include <misc/contract.hh>
#include <misc/indent.hh>

#include <misc/scoped-map.hh>

namespace misc
{

  // FIXedME: Some code was deleted here.
  
  template <typename Key, typename Data>
  scoped_map<Key, Data>::scoped_map()
  {
    scope_ = std::stack<std::map<Key, Data>>();
    scope_.push(std::map<Key, Data>());
  }
  
  template <typename Key, typename Data>
  scoped_map<Key, Data>::~scoped_map()
  {
    while(!(scope_.empty()))
      {
	scope_.pop();
      }
  }
  
  template <typename Key, typename Data>
  void scoped_map<Key, Data>::put(const Key& key, const Data& value)
  {
    scope_.top().insert_or_assign(key, value);
  }

  template <typename Key, typename Data>
  Data scoped_map<Key, Data>::get(const Key& key) const
  {
    auto i = scope_.top().find(key);
    if constexpr (std::is_pointer_v<Data>)
      {
	if (i == scope_.top().end())
	  return nullptr;
	return i->second;
      }
    else
      {
	if (i == scope_.top().end())
	  throw std::exception();
        return i->second;
      }
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_begin()
  {
    auto new_map = std::map<Key, Data>();
    for (auto i : scope_.top())
      new_map.insert_or_assign(i.first, i.second);
    scope_.push(new_map);
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_end()
  {
    scope_.pop();
  }

  template <typename Key, typename Data>
  std::ostream& scoped_map<Key, Data>::dump(std::ostream& ostr) const
  {
    for (auto i : scope_.top())
      {
	ostr << i.first << " is located " << i.second << std::endl; 
      }
    return ostr;
  }
  
  template <typename Key, typename Data>
  inline std::ostream&
  operator<<(std::ostream& ostr, const scoped_map<Key, Data>& tbl)
  {
    return tbl.dump(ostr);
  }

} // namespace misc
